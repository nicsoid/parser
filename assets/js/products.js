// require jQuery normally
const $ = require('jquery');
// create global $ and jQuery variables
global.$ = global.jQuery = $;

//************* first call
//create articuls
$(document).on('click', '#startarticul', function () {

    $.ajax({
        url: '/product-articul',
        type: "POST",
        dataType: "json",
        async: true,
        beforeSend: function beforeSend() {
            console.log("ready to start articul");
        }
    });
    return false;
});
//create xml for prom
$(document).on('click', '#creatpromxml', function () {

    $.ajax({
        url: '/product-export-prom',
        type: "POST",
        dataType: "json",
        async: true,
        beforeSend: function beforeSend() {
            console.log("ready to export to Prom");
        }
    });
    return false;
});

$(document).ready(function () {

    $(document).ajaxSend(function (e, xhr, opt) {
        if (opt.url === "/product-articul") {
            //parse products
            $('#startarticuls').text("Creating Articuls");
        }
    });

    $(document).ajaxSuccess(function (event, xhr, settings) {

        if (settings.url === "/product-articul") {
            responce = $.parseJSON(xhr.responseText);
            console.log("Finished Articuls: " + responce.numproducts);
            $('#progress').html("For " + responce.numproducts + " Articuls: " + responce.articuls + "<br>");
        }
        if (settings.url === "/product-export-prom") {
            responce = $.parseJSON(xhr.responseText);
            console.log("Finished Articuls: " + responce.link);
            $('#progress').html("XML file for Prom <a href='http://parser.ourstyle.com.ua/" + responce.link + "'>http://parser.ourstyle.com.ua/" + responce.link + "</a> Total Products: " + responce.products_counter + ". Unique articuls: " + responce.products_articuls + "<br>");
        }
    });
});

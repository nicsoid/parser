// require jQuery normally
const $ = require('jquery');
// create global $ and jQuery variables
global.$ = global.jQuery = $;

//************* first call
//first call - run parser on all selected categories: get pages -> get links -> get products -> get images
$(document).on('click', '#runselected', function(){
    //get categories selected
    var categories = [];
    $.each($("input[name='categories']:checked"), function(){
        categories.push($(this).val());
    });
    $('#progress').text("Categories to parse: " + categories.join(", "));//show status message
    $('#runselected').text('PARSING ...');//change button text
    $('#runselected').attr('disabled','disabled');//disable button

    //call parsing
    $.ajax({
        url:'/parser-run-all',
        type: "POST",
        dataType: "json",
        data: {
            "categories": categories.join(","),
            "id": categories[0],//current category
            "stage": 1,
            "params": "0,0,0,0,0"//[0]-numpages,[1]-current page,[2]-is it first request to parse products or not [0,1],[3] - numproducts,[4] - products left,[5] - products left for images to parse
        },
        async: true,
        beforeSend:function ()
        {
            console.log("Starting to parse all categories ");
        }
    });

});
//select all categories

$(document).on('click', '#selectall', function() {
    $.each($("input[name='categories']"), function(){

        if($(this).prop('checked'))
            $(this).attr('checked',false);
        else
            $(this).attr('checked',true);
    });
});
//get number of pages in category
$(document).on('click', '#startcat', function(){
    
    var id = $('#catid').val();
    $.ajax({
        url:'/parser-indexer-pages',
        type: "POST",
        dataType: "json",
        data: {
            "id": id
        },
        async: true,
        beforeSend:function ()
        {
            console.log("ready to send "+id);
        }
    });
    return false;
});
//get product links
$(document).on('click', '#startindex', function(){
    var that = $(this);
    var id = $('#catid').val();
    var numpages = $('#cat-pages-'+id).val();
    $.ajax({
        url:'/parser-indexer-links',
        type: "POST",
        dataType: "json",
        data: {
            "id": id,
            "numpages": numpages,
            "page_to_parse": 1//first request
        },
        async: true,
        beforeSend:function ()
        {
            console.log("ready to get links "+id);
        }
    });
    return false;
});
//parse products
$(document).on('click', '#startparse', function(){
    var id = $('#catid').val();

    $.ajax({
        url:'/parser-indexer-products',
        type: "POST",
        dataType: "json",
        data: {
            "id": id,
            "pages_to_parse": 10,//first request
            "first_request":1
        },
        async: true,
        beforeSend:function ()
        {
            console.log("ready to parse products "+id);
        }
    });
    return false;
});
//get images
$(document).on('click', '#startimg', function(){
    var id = $('#catid').val();
    $.ajax({
        url:'/parser-images',
        type: "POST",
        dataType: "json",
        data: {
            "id": id
        },
        async: true,
        beforeSend:function ()
        {
            console.log("ready to copy images "+id);
        }
    });
    return false;
});
/*****************************************************************/
//get number of pages in category
$(document).on('click', '#form_collectparameters', function(){
    var id = $('#form_catid').val();
    $.ajax({
        url:'/collect-params',
        type: "POST",
        dataType: "json",
        data: {
            "id": id
        },
        async: true,
        beforeSend:function ()
        {
            console.log("Start getting parameters cat "+id);
        }
    });
    return false;
});


$(document).ready(function(){

    $(document).ajaxSend(function(e, xhr, opt){
        if(opt.url==="/parser-indexer-pages") {//get number of pages in category
            console.log("Start get numOfPages cat " + opt.data.slice(3, 100));
            //$('#log-' + opt.data.slice(3, 100)).html("Indexing cat=" + opt.data.slice(3, 100));
        }
        if(opt.url==="/parser-indexer-links") {//get products link
            //console.log("Indexing cat=" + opt.data.slice(3, 100))
            //$('#log-' + opt.data.slice(3, 100)).html("Indexing cat=" + opt.data.slice(3, 100));
        }
        if(opt.url==="/parser-indexer-products") {//parse products
            console.log("Parsing products=" + opt.data.slice(3, 100));
            //$('#log-' + opt.data.slice(3, 100)).html("Indexing cat=" + opt.data.slice(3, 100));
        }
        if(opt.url==="/collect-params") {//parse products
            $('#form_collectparameters').text("Collecting...");
            //$('#log-' + opt.data.slice(3, 100)).html("Indexing cat=" + opt.data.slice(3, 100));
        }
        if(opt.url==="/parser-images") {//parse products
            console.log("Copying images");
        }

    });

    $( document ).ajaxSuccess(function( event, xhr, settings ) {

        if(settings.url==="/parser-indexer-pages") {
            responce = $.parseJSON(xhr.responseText);
            console.log("Finished " + responce.category + "; form=" + responce.formId + "; pages=" + responce.numpages);
            $('#cat-pages-' + responce.category).val(responce.numpages);
            $('#progress').append("Category "+responce.category + ". Pages with products: "+responce.numpages+"<br>");
        }
        /*******************/
        if(settings.url==="/parser-indexer-links") {
            responce = $.parseJSON(xhr.responseText);
            console.log("Finished " + responce.category + "; page "+responce.page_to_parse+"; pages=" + responce.numpages);
           //$('#cat-pages-' + responce.category).val(responce.numpages);
            $('#progress').append("Category "+responce.category + ". Pages with products: "+responce.numpages+"; parsed pages:"+responce.page_to_parse+" New Products: "+responce.result+", To Update: "+responce.toupdate+"<br>");
            responce.page_to_parse++;

            if (responce.page_to_parse <= responce.numpages)
                $.ajax({
                    url: '/parser-indexer-links',
                    type: "POST",
                    dataType: "json",
                    data: {
                        "id": responce.category,
                        "numpages": responce.numpages,
                        "page_to_parse": responce.page_to_parse
                    },
                    async: true,
                    beforeSend: function () {
                        console.log("ready to parse category for links " + responce.category+", page "+ responce.page_to_parse + ", total: "+responce.numpages);
                    }
                });
        }
        /**********************/
        if(settings.url==="/parser-indexer-products") {
            responce = $.parseJSON(xhr.responseText);
            console.log("Finished " + responce.id + "; products "+responce.numproducts);
            //$('#cat-pages-' + responce.category).val(responce.numpages);
            $('#progress').text("Category "+responce.id + ". Products parsed: "+responce.numproducts+"; To parse:"+responce.products_left);

            if (responce.proceed)
                $.ajax({
                    url:'/parser-indexer-products',
                    type: "POST",
                    dataType: "json",
                    data: {
                        "id": responce.id,
                        "pages_to_parse": 5//first request
                    },
                    async: true,
                    beforeSend:function ()
                    {
                        console.log("Parsing next 10 products cat="+responce.id);
                    }
                });
        }
        /*************************/
        if(settings.url==="/collect-params") {
            responce = $.parseJSON(xhr.responseText);
            console.log("Finished parameters");
            $('#form_params_parsed').text(responce.params);
            $('#form_collectparameters').text("Params collected");
            //$('#progress').append("Category "+responce.category + ". Pages with products: "+responce.numpages+"<br>");
        }
        /*************************/
        if(settings.url==="/parser-images") {
            responce = $.parseJSON(xhr.responseText);
            console.log("Copying images");
            //$('#form_collectparameters').text("Params collected");
            if(responce.proceed)
            $.ajax({
                url:'/parser-images',
                type: "POST",
                dataType: "json",
                data: {
                    "id": responce.id
                },
                async: true,
                beforeSend:function ()
                {
                    console.log("Continue copying images "+responce.id);
                }
            });

        }

        /************ AUTOMATIC PARSER *************/
        if(settings.url==="/parser-run-all") {
            responce = $.parseJSON(xhr.responseText);
            //console.log("Running automatic parser");
            categories = responce.categories;
            currentCategory = responce.currentCategory;
            stage = responce.stage;
            params = responce.params;

            //call parsing
            if(stage>0)
            $.ajax({
                url:'/parser-run-all',
                type: "POST",
                dataType: "json",
                data: {
                    "categories": categories.join(","),
                    "id": currentCategory,//current category
                    "stage": stage,
                    "params": params.join(",")//[0]-numpages,[1]-current page,[2]-is it first request to parse products or not [0,1],[3] - numproducts,[4] - products left,[5] - products left for images to parse
                },
                async: true,
                beforeSend:function ()
                {
                    console.log("Starting to parse all categories "+params.join(","));
                }
            });

            //show status messages
            switch (stage) {
                case 1: $('#progress').text("Stage 1: Parse for Cat Pages. Categories:"+responce.categories.join(",")+". Category: "+responce.currentCategory + ". Num pages of category: "+params[0]);
                    break;
                case 2: $('#progress').text("Stage 2: Get Product Links. Categories:"+responce.categories.join(",")+". Category: "+responce.currentCategory + ". Num pages of category: "+params[0]+". Page to parse: "+params[1]);
                    break;
                case 3: $('#progress').text("Stage 3: Get Products. Categories:"+responce.categories.join(",")+". Category: "+responce.currentCategory + ". Products TODO: "+params[4]);
                    break;
                case 4: $('#progress').text("Stage 4: Get Images. Categories:"+responce.categories.join(",")+". Category: "+responce.currentCategory + ". Products TODO: "+params[5]);
                    break;
                case 0: $('#progress').append("Finished");
                    break;
            }
            //$('#progress').text("Categories:"+responce.categories.join(",")+". Category: "+responce.currentCategory + ". Num pages of category: "+params[0]+". Stage: "+stage+". Page to parse: "+params[1]+". Total products: "+params[3]+". Products left: "+params[4]);
        }



    });
});

//
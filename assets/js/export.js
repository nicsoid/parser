// require jQuery normally
const $ = require('jquery');
// create global $ and jQuery variables
global.$ = global.jQuery = $;

//get gategories
$(document).on('click', '#exportcats', function(){

    $.ajax({
        url:'/export-categories',
        type: "POST",
        dataType: "json",
        data: {
            "id": 0
        },
        async: true,
        beforeSend:function ()
        {
            console.log("ready to export cats ");
        }
    });
    return false;
});
//get products
$(document).on('click', '#exportprods', function(){

    $.ajax({
        url:'/export-products',
        type: "POST",
        dataType: "json",
        data: {
            "id": 0
        },
        async: true,
        beforeSend:function ()
        {
            console.log("ready to export cats ");
        }
    });
    return false;
});

$(document).ready(function(){

    $( document ).ajaxSuccess(function( event, xhr, settings ) {

        if(settings.url==="/export-categories") {
            responce = $.parseJSON(xhr.responseText);
            $('#ajax-results').append("<a href='/"+responce.link+"'>Categories for TiModna</a>");
            //$('#progress').append("Category "+responce.category + ". Pages with products: "+responce.numpages+"<br>");
        }

    });
    $( document ).ajaxSuccess(function( event, xhr, settings ) {

        if(settings.url==="/export-products") {
            responce = $.parseJSON(xhr.responseText);
            $('#ajax-results').append("<br><a href='/"+responce.link+"'>Products for TiModna Ready</a>");
            //$('#progress').append("Category "+responce.category + ". Pages with products: "+responce.numpages+"<br>");
        }

    });

});
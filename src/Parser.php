<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 21.09.2018
 * Time: 0:27
 */

namespace App;


use App\Entity\CategoriesToParse;
use App\Entity\LinksToParse;

interface Parser
{

    /**
     * @param CategoriesToParse $category
     * @return mixed
     */
    public function getNumberOfPagesInCategory(CategoriesToParse $category);

    /**
     * @param CategoriesToParse $category
     * @param $page_to_parse
     * @return mixed
     */
    public function getCategoryProductLinks(CategoriesToParse $category, $page_to_parse);

    /**
     * @param LinksToParse $link
     * @param $xml
     * @return mixed
     */
    public function getProduct(LinksToParse $link, $xml);
    /*
     * product
     * array('name','price',params[['param'=>value],...],'description','sizes',images[])
     * */
}
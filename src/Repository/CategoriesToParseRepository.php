<?php

namespace App\Repository;

use App\Entity\CategoriesToParse;
use App\Entity\Programs;
use App\Entity\Categories;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\Lexer;
use Symfony\Bridge\Doctrine\RegistryInterface;


/**
 * @method CategoriesToParse|null find($id, $lockMode = null, $lockVersion = null)
 * @method CategoriesToParse|null findOneBy(array $criteria, array $orderBy = null)
 * @method CategoriesToParse[]    findAll()
 * @method CategoriesToParse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CategoriesToParseRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, CategoriesToParse::class);
    }

//    /**
//     * @return Product[] Returns an array of Product objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */


    /**
     * @return mixed
     */
    public function findAllCategories(): array
    {

       return $this->getEntityManager()->createQuery('SELECT c.id, c.cat_to_parse_name, c.cat_to_parse_link, c.date_parsed,c.product_count, 
       cm.name as cat_main_name,p.name as program_name FROM App\Entity\CategoriesToParse c 
LEFT JOIN App\Entity\Programs p with c.program = p.id 
Left Join App\Entity\Categories cm with c.cat_main_id=cm.id')->getResult();

    }

    /**
     * @return mixed
     */
    public function getParametersMapping(): array
    {
        $parameters_map=array();
        $categories = $this->findAll();
        foreach ($categories as $category)
        {
            //$parameters_map[$category->getId()]=$category->getParamsMapping();
            if($category->getParamsMapping()!="") {
                $chunks = array_chunk(preg_split('/(=>|\n)/', $category->getParamsMapping()), 2);
                $parameters_map[$category->getId()] = array_combine(array_column($chunks, 0), array_column($chunks, 1));
            }
        }
        return $parameters_map;

    }

    public function deleteProductsAndLinks($categoryId){

        $this->getEntityManager()->createQuery('Delete from App\Entity\LinksToParse l where l.category_to_parse='.$categoryId)->execute();
        $this->getEntityManager()->createQuery('Delete from App\Entity\Product p where p.cat_parsed='.$categoryId)->execute();

    }

    /**
     * @return array
     */
    public function getMarkups(): array
    {
        $categories = $this->findAll();
        $markups=[];
        foreach ($categories as $category)
        {
            $markup=$category->getMarkup() >0 ? $category->getMarkup() : 15;//15% markup default!!!!!
            $markups[$category->getId()]=$markup;
        }
        return $markups;

    }
}

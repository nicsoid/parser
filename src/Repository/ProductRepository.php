<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\Query\ResultSetMapping;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }

//    /**
//     * @return Product[] Returns an array of Product objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    
    public function findOneByModel($model): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.model = :val')
            ->andWhere('p.articul IS NOT NULL')
            ->setParameter('val', $model)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    public function findOneByName($name): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.name = :val')
            ->andWhere('p.articul IS NOT NULL')
            ->setParameter('val', $name)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function getProgramId($productId)
    {
        $result = $this->getEntityManager()->createQuery('SELECT c.program FROM App\Entity\CategoriesToParse c INNER JOIN App\Entity\Product p with c.id = p.cat_parsed and p.id='.$productId)->getResult();
        return $result[0]["program"];
    }
    
    /**
     * @return mixed
     */
    public function findForProm(){

        return $this->getEntityManager()->createQuery('SELECT p.id, p.name, p.description, p.price,p.params, 
           p.sizes,p.images_timodna as images,p.articul,p.status,p.cat_main,p.cat_parsed,c.promua as promua FROM App\Entity\Product p 
LEFT JOIN App\Entity\Categories c with c.id = p.cat_main where p.status=1')->getResult();

    }
    /**
     * @return mixed
     */
    public function findForTiModnaComUa(){

        return $this->getEntityManager()->createQuery('SELECT p.id, p.name, p.description, p.price,p.params, 
           p.sizes,p.images_timodna as images,p.articul,p.status,p.cat_main,p.cat_parsed,c.name as category FROM App\Entity\Product p 
LEFT JOIN App\Entity\Categories c with c.id = p.cat_main where p.status=1')->getResult();

    }
    /** Function gets array of articuls
     * @return mixed
     */
    public function findArticuls(int $status=1){

        if($status<0)//get all products
            return $this->getEntityManager()->createQuery('SELECT distinct p.articul FROM App\Entity\Product p')->getResult();
        else
            return $this->getEntityManager()->createQuery('SELECT distinct p.articul FROM App\Entity\Product p where p.status=1')->getResult();

    }
    /**
     * @return array
     */
    public function findProductsByArticul($articul){
        return $this->getEntityManager()->createQuery('SELECT p.id, p.name, p.description, p.price, p.params, 
           p.sizes,p.images_timodna as images,p.articul,p.status,l.link,l.date_parsed FROM App\Entity\Product p 
INNER JOIN App\Entity\LinksToParse l Inner JOIN App\Entity\Categories as c with l.id = p.link and p.articul=\''.$articul.'\' and p.status=\'1\' and l.cat_main_id=c.id')->getResult();
    }
    /**
     * @return mixed
     */
    public function setStatusByCategoryToParse($category_to_parse,$status=10){
        return $this->getEntityManager()->createQuery('Update App\Entity\LinksToParse l set l.status=\''.$status.'\' where l.category_to_parse=\''.$category_to_parse.'\'')->getResult();
    }
    /**
     * @return mixed
     */
    public function setProductStatusByCategoryToParse($category_to_parse,$status=10){
        return $this->getEntityManager()->createQuery('Update App\Entity\Product p set p.status=\''.$status.'\' where p.cat_parsed=\''.$category_to_parse.'\'')->getResult();
    }
    /**
     * @return mixed
     */
    public function getUsedImages($status=1){
        return $this->getEntityManager()->createQuery('SELECT p.id, p.images_timodna FROM App\Entity\Product p 
Where p.status=\'1\'')->getResult();

    }

    /**
     * @return mixed
     */
    public function setImagesTiModnaNull($productId,$images_timodna=NULL){
        return $this->getEntityManager()->createQuery('Update App\Entity\Product p set p.images_timodna=\''.$images_timodna.'\' where p.id=\''.$productId.'\'')->getResult();
    }
    /**
     * @return mixed
     */
    public function setAllImagesTiModnaNullByStatus($status=1,$images_timodna=NULL){
        return $this->getEntityManager()->createQuery('Update App\Entity\Product p set p.images_timodna=\''.$images_timodna.'\' where p.status!=\''.$status.'\'')->getResult();
    }

}

<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180923213020 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product ADD name VARCHAR(255) NOT NULL, ADD description LONGTEXT DEFAULT NULL, ADD price DOUBLE PRECISION NOT NULL, ADD cat_main INT NOT NULL, ADD cat_parsed INT NOT NULL, ADD date_parsed DATETIME NOT NULL, ADD link INT NOT NULL, ADD params LONGTEXT DEFAULT NULL, ADD sizes VARCHAR(255) DEFAULT NULL, ADD images LONGTEXT DEFAULT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP name, DROP description, DROP price, DROP cat_main, DROP cat_parsed, DROP date_parsed, DROP link, DROP params, DROP sizes, DROP images');
    }
}

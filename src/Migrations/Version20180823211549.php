<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180823211549 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE categories (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, parent INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE categories_to_parse (id INT AUTO_INCREMENT NOT NULL, cat_to_parse_name VARCHAR(255) NOT NULL, program INT NOT NULL, cat_to_parse_link VARCHAR(255) NOT NULL, date_parsed VARCHAR(255) NOT NULL, cat_main_id INT NOT NULL, product_count INT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE forms (id INT AUTO_INCREMENT NOT NULL, cat_to_parse_id INT NOT NULL, name VARCHAR(255) NOT NULL, cat_template LONGTEXT DEFAULT NULL, product_template LONGTEXT DEFAULT NULL, product_list_template LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE links_to_parse (id INT AUTO_INCREMENT NOT NULL, link VARCHAR(255) NOT NULL, cat_main_id INT NOT NULL, form_id INT NOT NULL, status SMALLINT NOT NULL, date_parsed DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE categories');
        $this->addSql('DROP TABLE categories_to_parse');
        $this->addSql('DROP TABLE forms');
        $this->addSql('DROP TABLE links_to_parse');
    }
}

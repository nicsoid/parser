<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 21.09.2018
 * Time: 1:20
 */

namespace App\Parser;


use App\Entity\CategoriesToParse;
use App\Entity\LinksToParse;
use App\Parser;

use Symfony\Component\DomCrawler\Crawler;
use Clue\React\Buzz\Browser;
use Psr\Http\Message\ResponseInterface;
use React\EventLoop\Factory;

class ParserArjen implements Parser
{
    private $pages_in_category;
    private $category_products_link;
    private $images;
    private $sizes;
    private $price;
    private $name;
    private $products;

    /**
     * @param $category
     */
    public function getNumberOfPagesInCategory(CategoriesToParse $category)
    {
        $this->pages_in_category = 1;//if there is ony 1 page, <pagination> is not present
        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink().'1')->then(function (ResponseInterface $response) {

            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('#pag')->children()->filter('a')->each(function ( Crawler $node) {//eq(0)->children()->

                $this->pages_in_category = ($this->pages_in_category > (int)$node->text()) ? $this->pages_in_category : (int)$node->text();
            });
        });
       
    $loop->run();
	sleep(10);

        return $this->pages_in_category;

    }

    public function getCategoryProductLinks(CategoriesToParse $category,$page_to_parse)
    {
        //$crawler =  new Crawler(file_get_contents($category->getCatToParseLink().$page_to_parse));
        //$crawler->filter('.catalog-block')->each(function (Crawler $node, $i) {
        //    $this->category_products_link[]=['info'=>str_ireplace("<br>"," ",$node->filter('.h2')->children()->text()),'link'=>$node->children()->attr("href")];
        //});

        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink().$page_to_parse)->then(function (ResponseInterface $response) {

            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('.product')->each(function (Crawler $node) {
                //if()$node->children()->eq(0)->filter('a')->children()->filter('.product_prodan_page')->text()
                $info=$node->children()->filter('a')->first()->text();
                //$node->children()->filter('.product1_title')->text()
                if(!preg_match('/Продано/s',$info))
                $this->category_products_link[]=['info'=>$node->children()->filter('.p_title')->text(),'link'=>preg_replace("/\?.*/","",$node->children()->filter('.p_title')->attr("href"))];
            });
        });
        $loop->run();
		sleep(10);
        return $this->category_products_link;

    }

    public function getProduct(LinksToParse $link, $xml)// returns array of product.
    {

        $crawler =  new Crawler($xml);
		try{
        $crawler->filter('url:contains("'.$link->getLink().'")')->siblings()->each(function (Crawler $node) {

            if($node->nodeName()=='price_retail')
                $this->price=$node->text();
            if($node->nodeName()=='name')
                $this->name=$node->text();

            //get colors and sizes and pictures
            if($node->nodeName()=='colors'){
                $this->products=$node->filter('color')->each(function (Crawler $node2) {//separate colors

                    $color = $node2->children()->filter('color_name')->text();

                    $images = $node2->children()->filter('color_rakurs')->each(function(Crawler $node3){
                        return $node3->text();
                    });
                    $images[] = $node2->children()->filter('color_picture')->text();

                    $sizes = $node2->children()->filter('color_size')->each(function (Crawler $node4){

                        $size=$node4->children()->eq(0)->text();
                        if((int)$node4->children()->eq(1)->text() > 0)//products count > 0 then get sizes and return product
                            return $size;

                    });
                    $sizes=array_filter($sizes);//remove empty sizes
                    
					if($sizes)
                        return array('color'=>$color,'images'=>$images,'sizes'=>implode(',',$sizes));

                });

            }

        });
			
		}catch(\Exception $e){
			return null;
		}
        $this->products=array_filter($this->products);

        preg_match("/(\d+)/s",$this->name,$name);
        $model=$name[0];
        preg_match("/(\D+)/s",$this->name,$name);
        $this->name=$name[0];

		try{
        	$content=file_get_contents($link->getLink());
		}catch(\Exception $e){
			sleep(2);
			try{
				$content=file_get_contents($link->getLink());
			}catch(\Exception $e){
				sleep(5);
			$content=file_get_contents($link->getLink());
				}
		}
		
        $crawler =  new Crawler($content);

        try{
            //$description = strip_tags( $crawler->filter('.unselect')->text());
			$description = $crawler->filterXPath('//div[@itemprop="description"]')->children()->eq(0)->children()->eq(0)->text();
			$description = strip_tags( preg_replace("/\"Аржен\"/","", $description ));
			
        }catch (\Exception $exception){
            $description = "";
        }

        //$model = $crawler->filter('meta')-> attr('content');
        		
		$params = $crawler->filterXPath('//div[@itemprop="description"]')->children()->eq(1)->each(function (Crawler $node) {

            $params = $node->filter('tr')->each(function (Crawler $node2) {
                return array('param'=>$node2->children()->eq(0)->text(),'value'=>trim($node2->children()->eq(2)->text()));
            });
            return array('param'=>$params);

        });
		
        $this->sizes = $this->images = "";//$params[0]['param']

        //build array of products $params[0]['param']
        $products = array();
        foreach ($this->products as $product) {
            array_push($params[0]['param'],array('param'=>'Цвет','value'=>$product["color"]));
            $products[] = ['name'=>$this->name,'price'=>$this->price,'params'=>$params[0]['param'],'description'=>$description,'sizes'=>$product['sizes'], 'images'=>$product['images'],'link'=>$link->getId(), 'model'=>$model];
            array_pop($params[0]['param']);
        }
		sleep(5);
		//var_dump($products);
        //return ['name'=>$this->name,'price'=>$this->price,'params'=>$this->products,'description'=>$description,'sizes'=>$this->sizes, 'images'=>$this->images,'link'=>$link->getId(), 'model'=>$model];
        return $products;

    }
}
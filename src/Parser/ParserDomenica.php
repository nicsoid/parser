<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 21.09.2018
 * Time: 1:20
 */

namespace App\Parser;


use App\Entity\CategoriesToParse;
use App\Entity\LinksToParse;
use App\Parser;

use Symfony\Component\DomCrawler\Crawler;
use Clue\React\Buzz\Browser;
use Psr\Http\Message\ResponseInterface;
use React\EventLoop\Factory;

class ParserDomenica implements Parser
{
    private $pages_in_category;
    private $category_products_link;
    private $images;
    private $sizes;
    private $price;

    /**
     * @param $category
     */
    public function getNumberOfPagesInCategory(CategoriesToParse $category)
    {
        $this->pages_in_category = 1;//if there is ony 1 page, <pagination> is not present
        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink())->then(function (ResponseInterface $response) {


            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('.pagination')->children()->each(function ( Crawler $node, $i) {

                $this->pages_in_category = ($this->pages_in_category > (int)$node->children()->text()) ? $this->pages_in_category : (int)$node->children()->text();
            });
        });
        $loop->run();

        return $this->pages_in_category;

    }

    public function getCategoryProductLinks(CategoriesToParse $category,$page_to_parse)
    {
        //$crawler =  new Crawler(file_get_contents($category->getCatToParseLink().$page_to_parse));
        //$crawler->filter('.catalog-block')->each(function (Crawler $node, $i) {
        //    $this->category_products_link[]=['info'=>str_ireplace("<br>"," ",$node->filter('.h2')->children()->text()),'link'=>$node->children()->attr("href")];
        //});

        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink().$page_to_parse)->then(function (ResponseInterface $response) {

            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('.product-wrapper')->children()->each(function (Crawler $node) {

                try{
                    $info = $node->children()->eq(3)->text();
                }catch (\Exception $exception){
                    $info = "";
                }
                try{
                    $link = $node->attr('href');
                }catch (\Exception $exception){
                    $link = NULL;
                }
                if($link!=NULL)
                $this->category_products_link[]=['info'=>$info,'link'=>$link];

            });
        });
        $loop->run();

        return $this->category_products_link;

    }

    public function getProduct(LinksToParse $link, $xml)
    {

        $content=file_get_contents($link->getLink());
        $crawler =  new Crawler($content);

        $prod_name = $crawler->filter('h1')->text();

        $model = $crawler->filter('.info-order-product')->children()->filter('.description')->children()->eq(0)->children()->eq(0)->filter('span')->text();
        $available = $crawler->filter('.info-order-product')->children()->filter('.description')->children()->eq(0)->children()->eq(1)->text();
        try{
        $colors = $crawler->filter('.info-order-product')->children()->filter('.colors')->children()->filter('li')->each(function (Crawler $node){
            return $node->attr('data-color');
        });
        }catch(\Exception $e){$colors = NULL;}

        if($available!="Есть в наличии")//if product is not available
            return NULL;

        //$this->price = $crawler->filter('.optprice')->text();
        //$this->price = str_ireplace(" грн.","",$this->price);

		try{
			$this->price = $crawler->filter('.new-price')->text();
			$this->price = trim(str_ireplace("грн.","",$this->price));
		}catch(\Exception $e){
			$this->price = $crawler->filter('.optprice')->text();
        	$this->price = str_ireplace(" грн.","",$this->price);
		}
        //get sizes size-item
        $crawler->filter('.product-extra')-> children()->filter('.size-item')->each(function (Crawler $node) {

            $this->sizes[] = $node->text();//depending on size price may vary
            /*TODO*/ //get price variations
            });
        try{
        $crawler->filter('.superfoto')->each(function (Crawler $node) {

            $image = $node->attr('data-large') ?? NULL;

            if($node->attr('src')!="" && $image) {
                    $this->images[] = strpos("domenica",$image)===FALSE ? "https://domenica.com.ua".$image : $image;
            }
            });
        }catch(\Exception $e){
            $this->images = NULL;
        }

        //description
        try{
            $params[0]['description'] = $crawler->filter('#tabs-1')->each(function (Crawler $node) {

                return strip_tags(trim($node->text()));

            });
        }catch (\Exception $e)
        {$params[0]['description'][0]="";}

        //parameters
        try{
            $params[0]['params'] = $crawler->filter('#tabs-2')->children()->filter('tbody')->children()->filter('tr')->each(function (Crawler $node) {

                return array('param'=>$node->children()->eq(0)->text(),'value'=>trim($node->children()->eq(1)->text()));

            });
        }catch (\Exception $e)
        {$params[0]['params']="";}

        if($colors)
        foreach ($colors as $color)
            $params[0]['params'][]= ['param'=>'Цвет', 'value'=>$color];
/*
echo $this->price;
echo $prod_name;
echo $model;
echo $available;
echo implode(',',$this->sizes);
echo implode(',',$this->images);
echo strip_tags(trim($params[0]['description'][0]));
var_dump($params[0]['params']);*/
        return ['name'=>$prod_name,'price'=>$this->price,'params'=>$params[0]['params'],'description'=>$params[0]['description'][0],'sizes'=>implode(',',$this->sizes), 'images'=>$this->images,'link'=>$link->getId(), 'model'=>$model];

    }
}
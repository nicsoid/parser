<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 27.07.2018
 * Time: 07:28
 */

namespace App\Parser;


use App\Entity\CategoriesToParse;
use App\Entity\LinksToParse;
use App\Parser;

use Symfony\Component\DomCrawler\Crawler;
use Clue\React\Buzz\Browser;
use Psr\Http\Message\ResponseInterface;
use React\EventLoop\Factory;

class ParserEPokalitsina implements Parser
{
    private $pages_in_category;
    private $category_products_link;
    private $images;
    private $sizes;
    private $price;

    /**
     * @param $category
     */
    public function getNumberOfPagesInCategory(CategoriesToParse $category)
    {
        $this->pages_in_category = 1;//if there is ony 1 page, <pagination> is not present
        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink())->then(function (ResponseInterface $response) {


            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('.links')->children()->each(function ( Crawler $node, $i) {
                $link=preg_replace("/.*page=/","",$node->attr("href"));

                $this->pages_in_category = ($this->pages_in_category > (int)$link) ? $this->pages_in_category : (int)$link;
            });
        });
        $loop->run();

        return $this->pages_in_category;

    }

    public function getCategoryProductLinks(CategoriesToParse $category,$page_to_parse)
    {
        //$crawler =  new Crawler(file_get_contents($category->getCatToParseLink().$page_to_parse));
        //$crawler->filter('.catalog-block')->each(function (Crawler $node, $i) {
        //    $this->category_products_link[]=['info'=>str_ireplace("<br>"," ",$node->filter('.h2')->children()->text()),'link'=>$node->children()->attr("href")];
        //});

        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink().$page_to_parse)->then(function (ResponseInterface $response) {

            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('.prod__wrapper')->each(function (Crawler $node) {

                try{
                    $info = $node->children()->eq(0)->children()->eq(1)->attr("title");
                }catch (\Exception $exception){
                    $info = "";
                }
                try{
                    $link = $node->children()->eq(0)->children()->eq(0)->attr("href");
                }catch (\Exception $exception){
                    $link = NULL;
                }
                if($link!=NULL)
                $this->category_products_link[]=['info'=>$info,'link'=>$link];

            });
        });
        $loop->run();

        return $this->category_products_link;

    }

    public function getProduct(LinksToParse $link, $xml)
    {

        //get data from XML file
        /*
		$crawler =  new Crawler($xml);
        $crawler->filter('url:contains("'.$link->getLink().'")')->siblings()->each(function (Crawler $node) {

            if($node->nodeName()=='price')
                $this->price=$node->text();
            if($node->nodeName()=='name')
                $this->name=$node->text();

            //get colors and sizes and pictures

        });*/

        $content=file_get_contents($link->getLink());
        $crawler =  new Crawler($content);

        $prod_name = trim( $crawler->filter('h1')->text() );
		$prod_name = preg_replace("/\s\d\d+-?\d?\d?\d?\d?\d?-?\d?\d?\d?/","",$prod_name);
        
		$model = $crawler->filter('.reward__italic')->text();
		$model = preg_replace("/.*: /","",$model);
        $available = trim( $crawler->filter('.stock')->text() );
        
		/*try{
        $colors = $crawler->filter('.option[65568981]')->children()->each(function (Crawler $node){
            if($node->attr('value')!="")
				return $node->attr('value');
        });
        }catch(\Exception $e){$colors = NULL;}
		*/
		try{
			
		$colors = $crawler->filter('.options')->children()->filter('select')->each(function (Crawler $node){
			
			//$text=trim($node->filter("b")->text());
			
			//if($text!="Размер:")
			//{
				$colors=$node->filter("option")->each(function (Crawler $node){
					if($node->filter("option")->attr('value')!="")
						return trim($node->filter("option")->text());
				});
			return $colors;
			//}
            
		});
			
		}catch(\Exception $e){$colors = NULL;}
		

        if($available!="Есть в наличии")//if product is not available
            return NULL;

        $this->price = $crawler->filter('.discount')->text();
        $this->price = preg_replace("/.*Опт\s/","",$this->price);
        $this->price = preg_replace("/\s\w*/","",$this->price);//$this->price = preg_replace("/$$/","",$this->price);
		if(strpos($this->price,'$') !== false){//price in dollars, then convert
		   $this->price = ceil(str_ireplace("$","",$this->price)*27);
		}

        try{
        //get sizes size-item
        $crawler->filter('.label__wrapper')-> children()->filter('label')->each(function (Crawler $node) {

            $this->sizes[] = trim($node->text());//depending on size price may vary
            /*TODO*/ //get price variations
            });
        }catch(\Exception $e){
            return null;
        }
        try{
        $crawler->filter('.image-additional')->filter('a')->each(function (Crawler $node) {

            //$image = $node->attr('data-large') ?? NULL;

            if($node->attr('data-image')!="" ) {
                    $this->images[] = $node->attr('data-image');
            }
            });
        }catch(\Exception $e){
            $this->images = NULL;
        }

        //description
        try{
            $params[0]['description'] = $crawler->filter('#tab-description')->each(function (Crawler $node) {
//echo $node->text();
				$text=trim($node->children()->eq(1)->text());
				$text=preg_replace("/модель\s\d+/","",$text);
                return $text;

            });
        }catch (\Exception $e)
        {$params[0]['description'][0]="";}

        //parameters
        try{
            $params[0]['params'] = $crawler->filter('#tab-attribute')->children()->filter('tbody')->children()->filter('tr')->each(function (Crawler $node) {

                return array('param'=>trim($node->children()->eq(0)->text()),'value'=>trim($node->children()->eq(1)->text()));

            });
        }catch (\Exception $e)
        {$params[0]['params']=NULL;}

        if($colors)
        foreach ($colors[0] as $color){//$string = ucfirst(strtolower($color));
			if(trim($color)!="")
            $params[0]['params'][]= ['param'=>'Цвет', 'value'=>$this->my_mb_ucfirst(trim(preg_replace("/\(.*\)/","",$color)))];
			//echo $color;
		}
//echo $this->price;
//echo $prod_name;
//echo $model;
//echo $available;
//echo implode(',',$this->sizes);
//echo implode(',',$this->images);
//echo strip_tags(trim($params[0]['description'][0]));
//var_dump($params[0]['params']);
//var_dump($params[0]['description'][0]);
		//var_dump($params[0]['description']);
        var_dump(['name'=>$prod_name,'price'=>$this->price,'params'=>$params[0]['params'],'description'=>$params[0]['description'][0],'sizes'=>implode(',',$this->sizes), 'images'=>$this->images,'link'=>$link->getId(), 'model'=>$model]);
        return ['name'=>$prod_name,'price'=>$this->price,'params'=>$params[0]['params'],'description'=>$params[0]['description'][0],'sizes'=>implode(',',$this->sizes), 'images'=>$this->images,'link'=>$link->getId(), 'model'=>$model];

    }
	
	private function my_mb_ucfirst($str) {
    	$str = mb_strtolower($str);
		$fc = mb_strtoupper(mb_substr($str, 0, 1));
    	return $fc.mb_substr($str, 1);
}
	
}
<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 21.09.2018
 * Time: 1:20
 */

namespace App\Parser;


use App\Entity\CategoriesToParse;
use App\Entity\LinksToParse;
use App\Parser;

use Symfony\Component\DomCrawler\Crawler;
use Clue\React\Buzz\Browser;
use Psr\Http\Message\ResponseInterface;
use React\EventLoop\Factory;

class ParserModniyOstrov implements Parser
{
    private $pages_in_category;
    private $category_products_link;
    private $images;
    private $sizes;
    private $price;

    /**
     * @param $category
     */
    public function getNumberOfPagesInCategory(CategoriesToParse $category)
    {
        $this->pages_in_category = 1;//if there is ony 1 page, <pagination> is not present
        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink().'1')->then(function (ResponseInterface $response) {

            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('.pagination-wrapper')->children()->children()->each(function ( Crawler $node, $i) {

                $this->pages_in_category = ($this->pages_in_category > (int)$node->text()) ? $this->pages_in_category : (int)$node->text();
            });
        });
        $loop->run();

        return $this->pages_in_category;

    }

    public function getCategoryProductLinks(CategoriesToParse $category,$page_to_parse)
    {
        //$crawler =  new Crawler(file_get_contents($category->getCatToParseLink().$page_to_parse));
        //$crawler->filter('.catalog-block')->each(function (Crawler $node, $i) {
        //    $this->category_products_link[]=['info'=>str_ireplace("<br>"," ",$node->filter('.h2')->children()->text()),'link'=>$node->children()->attr("href")];
        //});

        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink().$page_to_parse)->then(function (ResponseInterface $response) {

            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('.listing-holder')->children()->filter('.title-item')->each(function (Crawler $node, $i) {
                if($node->attr("itemprop"))
                $this->category_products_link[]=['info'=>$node->attr("title"),'link'=>"https://modniy-ostrov.com".$node->attr("href")];
            
                
            });
        });
        $loop->run();

        return $this->category_products_link;

    }

    public function getProduct(LinksToParse $link, $xml)
    {

        $crawler =  new Crawler($xml);
        $crawler->filter('url:contains("'.$link->getLink().'")')->siblings()->each(function (Crawler $node, $i) {
            if($node->nodeName()=='picture')
                $this->images[]=$node->text();
            if($node->nodeName()=='price')
                $this->price=$node->text();

            if($node->nodeName()=='param')
                if($node->attr('name')=="Размер")
                    $this->sizes=$node->text();

        });

        $content=file_get_contents($link->getLink());
        $crawler =  new Crawler($content);

        $prod_name = $crawler->filter('h1')->text();
        preg_match("/p(\d+)\//s",$link->getLink(),$model);
        $model=$model[1];
        $params = $crawler->filter('.description-block')-> each(function (Crawler $node, $i) {

            $description = $node->filter('.mb25')->text();

            $params = $node->filter('.mb6')->each(function (Crawler $node2, $i) {
                return array('param'=>$node2->children()->eq(0)->text(),'value'=>trim($node2->children()->eq(1)->text()));
            });
            return array('description'=>$description,'params'=>$params);

        });

        return ['name'=>$prod_name,'price'=>$this->price,'params'=>$params[0]['params'],'description'=>$params[0]['description'],'sizes'=>$this->sizes, 'images'=>$this->images,'link'=>$link->getId(), 'model'=>$model];

    }
}
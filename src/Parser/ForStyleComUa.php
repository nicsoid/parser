<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 21.09.2018
 * Time: 1:20
 */

namespace App\Parser;


use App\Entity\CategoriesToParse;
use App\Entity\LinksToParse;
use App\Parser;

use Symfony\Component\DomCrawler\Crawler;
use Clue\React\Buzz\Browser;
use Psr\Http\Message\ResponseInterface;
use React\EventLoop\Factory;

class ForStyleComUa implements Parser
{
    private $pages_in_category;
    private $category_products_link;
    private $images;
    private $sizes;
    private $price;

    /**
     * @param $category
     */
    public function getNumberOfPagesInCategory(CategoriesToParse $category)
    {
        $this->pages_in_category = 1;//if there is ony 1 page, <pagination> is not present
        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink())->then(function (ResponseInterface $response) {

            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('.pagination')->children()->each(function ( Crawler $node, $i) {

                $this->pages_in_category = ($this->pages_in_category > (int)$node->children()->text()) ? $this->pages_in_category : (int)$node->children()->text();
            });
        });
        $loop->run();

        return $this->pages_in_category;

    }

    public function getCategoryProductLinks(CategoriesToParse $category,$page_to_parse)
    {
        //$crawler =  new Crawler(file_get_contents($category->getCatToParseLink().$page_to_parse));
        //$crawler->filter('.catalog-block')->each(function (Crawler $node, $i) {
        //    $this->category_products_link[]=['info'=>str_ireplace("<br>"," ",$node->filter('.h2')->children()->text()),'link'=>$node->children()->attr("href")];
        //});

        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink().$page_to_parse)->then(function (ResponseInterface $response) {

            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('.product-layout')->each(function (Crawler $node, $i) {
                $this->category_products_link[]=['info'=>str_ireplace("<br>"," ",$node->filter('.caption')->children()->text()),'link'=>$node->filter('.caption')->children()->attr("href")];
            });
        });
        $loop->run();

        return $this->category_products_link;

    }

    public function getProduct(LinksToParse $link, $xml)
    {
/*
        $crawler =  new Crawler($xml);
        $crawler->filter('url:contains("'.$link->getLink().'")')->siblings()->each(function (Crawler $node, $i) {
            if($node->nodeName()=='picture')
                $this->images[]=$node->text();
            if($node->nodeName()=='price')
                $this->price=$node->text();

            if($node->nodeName()=='param')
                if($node->attr('name')=="Размер")
                    $this->sizes=$node->text();

        });
*/      $content = "";
        try{
            $content=file_get_contents($link->getLink());
        }catch(\Exception $exception){
            sleep(5);
            try{
                $content=file_get_contents($link->getLink());
            }catch(\Exception $exception){

            }
        }


        preg_match('/<meta itemprop="price" content="(\d*)"/Uis',$content,$price);
        $this->price = $price[1];
        preg_match('/<meta itemprop="model" content="(.*)"/Uis',$content,$model_tmp);
        $model = $model_tmp[1];

        $crawler =  new Crawler($content);

        $prod_name = $crawler->filter('h1')->text();
        preg_match("/(\D+)/s",$prod_name,$name);
        $prod_name = $name[0];//get name without digits

        //$model = $crawler->filter('.model')->attr('content');//model

        $params = $crawler->filter('#tab-description')->each(function (Crawler $node, $i) {//get tab description and parse for
            $description = trim($node->children()->eq(1)->children()->eq(0)->text());
            $description = preg_replace('/(\d)/s',"",$description);
            $all_params = $node->filter('p')->text();
            if(strlen($all_params)<100)
                $all_params = $node->filter('p')->eq(1)->text();
            //get params
            $all_params = explode(';',strip_tags($all_params));
            foreach ($all_params as $value)
            {
                $tmp = explode(':',$value);
                //if(isset($tmp[1]))
                $all_params_array[]=array('param'=>trim($tmp[0]), 'value'=>trim($tmp[1]));

                if(trim($tmp[0])=="размерный ряд")
                    $this->sizes=trim($tmp[1]);
                $tmp=NULL;
            }
            return array('description'=>$description,'params'=>$all_params_array);
        });

        $crawler->filter('.img-circle')->each(function (Crawler $node, $i) {
            $this->images[] = str_ireplace("70x70","650x650",$node->attr('src'));
        });

        return ['name'=>$prod_name,'price'=>$this->price,'params'=>$params[0]["params"],'description'=>$params[0]["description"],'sizes'=>$this->sizes, 'images'=>$this->images,'link'=>$link->getId(), 'model'=>$model];

    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 21.09.2018
 * Time: 19:49
 */

namespace App\Parser;


use App\Entity\CategoriesToParse;
use App\Entity\LinksToParse;
use App\Parser;
use Fig\Link\Link;
use Symfony\Component\DomCrawler\Crawler;

class ParserTytModno implements Parser
{
    private $pages_in_category;
    private $category_products_link;

    public function getNumberOfPagesInCategory(CategoriesToParse $category)
    {
        $crawler =  new Crawler(file_get_contents($category->getCatToParseLink()));
        $crawler->filter('.pagination')->children()->filter('a')->each(function ( Crawler $node, $i) {
            $this->pages_in_category = ($this->pages_in_category >  (int)$node->text()) ? $this->pages_in_category : (int)$node->text();
        });
        return $this->pages_in_category;
        // TODO: Implement getNumberOfPagesInCategory() method.
    }

    public function getCategoryProductLinks(CategoriesToParse $category, $page_to_parse)
    {
        $crawler =  new Crawler(file_get_contents($category->getCatToParseLink().$page_to_parse));
        $crawler->filter('.image')->each(function (Crawler $node, $i) {

            $this->category_products_link[]=['info'=>$node->children()->eq(0)->children()->eq(0)->attr('alt'),'link'=>$node->children()->eq(0)->attr('href')];
        });
        return $this->category_products_link;
        // TODO: Implement getCategoryProductLinks() method.
    }

    public function getProduct(LinksToParse $link, $xml="")
    {
        $crawler =  new Crawler($xml);
        $crawler->filter('url:contains("'.$link->getLink().'")')->siblings()->each(function (Crawler $node, $i) {
            if($node->nodeName()=='picture')
                $this->images[]=$node->text();
            if($node->nodeName()=='price')
                $this->price=$node->text();

            if($node->nodeName()=='param')
                if($node->attr('name')=="Размер")
                    $this->sizes=$node->text();

        });

        $content=file_get_contents($link->getLink());
        $crawler =  new Crawler($content);

        $prod_name = $crawler->filter('h1')->text();

        $params = $crawler->filter('.description-block')-> each(function (Crawler $node, $i) {

            $description = $node->filter('.mb25')->text();

            $params = $node->filter('.mb6')->each(function (Crawler $node2, $i) {
                return array('param'=>$node2->children()->eq(0)->text(),'value'=>trim($node2->children()->eq(1)->text()));
            });
            return array('description'=>$description,'params'=>$params);

        });

        return ['name'=>$prod_name,'price'=>$this->price,'params'=>$params[0]['params'],'description'=>$params[0]['description'],'sizes'=>$this->sizes, 'images'=>$this->images,'link'=>$link->getId()];

        // TODO: Implement getProduct() method.
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 21.09.2018
 * Time: 19:50
 */

namespace App\Parser;


use App\Entity\CategoriesToParse;
use App\Entity\LinksToParse;
use App\Parser;

class ParserTimeOfStyle implements Parser
{
    private $pages_in_category;
    private $category_products_link;

    public function getNumberOfPagesInCategory(CategoriesToParse $category)
    {
        $crawler =  new Crawler(file_get_contents($category->getCatToParseLink()));
        $crawler->filter('.pagination_buttons')->children()->children()->filter('a')->each(function ( Symfony\Component\DomCrawler\Crawler $node, $i) {
            $this->pages_in_category = ($this->pages_in_category >  (int)$node->text()) ? $this->pages_in_category : (int)$node->text();
        });
        return $this->pages_in_category;
        // TODO: Implement getNumberOfPagesInCategory() method.
    }

    public function getCategoryProductLinks(CategoriesToParse $category, $page_to_parse)
    {
        $crawler =  new Crawler(file_get_contents($category->getCatToParseLink().$page_to_parse));
        $crawler->filter('.name')->each(function (Crawler $node, $i) {
            $this->category_products_link[]=['info'=>$node->children()->text(),'link'=>$node->children()->attr("href")];
        });
        return $this->category_products_link;
        // TODO: Implement getCategoryProductLinks() method.
    }

    public function getProduct(LinksToParse $link, $xml="")
    {
        // TODO: Implement getProduct() method.
    }
}
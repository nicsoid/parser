<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 21.09.2018
 * Time: 1:20
 */

namespace App\Parser;


use App\Entity\CategoriesToParse;
use App\Entity\LinksToParse;
use App\Parser;

use Symfony\Component\DomCrawler\Crawler;
use Clue\React\Buzz\Browser;
use Psr\Http\Message\ResponseInterface;
use React\EventLoop\Factory;

class ParserLShop implements Parser
{
    private $pages_in_category;
    private $category_products_link;
    private $images;
    private $sizes;
    private $price;
    private $name;
    private $products;
	private $articul;
	private $color;
	private $brand;
	private $country;
	private $description;
    /**
     * @param $category
     */
    public function getNumberOfPagesInCategory(CategoriesToParse $category)
    {
        $this->pages_in_category = 1;//if there is ony 1 page, <pagination> is not present
        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink().'1')->then(function (ResponseInterface $response) {

            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('div .pagination')->children()->filter('a')->each(function ( Crawler $node) {//eq(0)->children()->

                $this->pages_in_category = ($this->pages_in_category > (int)$node->text()) ? $this->pages_in_category : (int)$node->text();
            });
        });
        $loop->run();

        return $this->pages_in_category;

    }

    public function getCategoryProductLinks(CategoriesToParse $category,$page_to_parse)
    {
        //$crawler =  new Crawler(file_get_contents($category->getCatToParseLink().$page_to_parse));
        //$crawler->filter('.catalog-block')->each(function (Crawler $node, $i) {
        //    $this->category_products_link[]=['info'=>str_ireplace("<br>"," ",$node->filter('.h2')->children()->text()),'link'=>$node->children()->attr("href")];
        //});

        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink().$page_to_parse)->then(function (ResponseInterface $response) {

            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('div .product')->each(function (Crawler $node) {
                //if()$node->children()->eq(0)->filter('a')->children()->filter('.product_prodan_page')->text()
                $info=$node->children()->filter('h4')->text();
                //$node->children()->filter('.product1_title')->text()
				$link=$node->children()->filter('.action-control')->children()->filter('a')->attr("href");
                $this->category_products_link[]=['info'=>$info,'link'=>$link];
            });
        });
        $loop->run();

        return $this->category_products_link;

    }

    public function getProduct(LinksToParse $link, $xml)// returns array of product.
    {
		//colors: beliy, cherniy: take first part of color string, also, add color string to description
/*
        $crawler =  new Crawler($xml);
        $crawler->filter('url:contains("'.$link->getLink().'")')->siblings()->each(function (Crawler $node) {

            if($node->nodeName()=='price_retail')
                $this->price=$node->text();
            if($node->nodeName()=='name')
                $this->name=$node->text();

            //get colors and sizes and pictures
            if($node->nodeName()=='colors'){
                $this->products=$node->filter('color')->each(function (Crawler $node2) {//separate colors

                    $color = $node2->children()->filter('color_name')->text();

                    $images = $node2->children()->filter('color_rakurs')->each(function(Crawler $node3){
                        return $node3->text();
                    });
                    $images[] = $node2->children()->filter('color_picture')->text();

                    $sizes = $node2->children()->filter('color_size')->each(function (Crawler $node4){

                        $size=$node4->children()->eq(0)->text();
                        if((int)$node4->children()->eq(1)->text() > 0)//products count > 0 then get sizes and return product
                            return $size;

                    });
                    $sizes=array_filter($sizes);//remove empty sizes
                    if($sizes)
                        return array('color'=>$color,'images'=>$images,'sizes'=>implode(',',$sizes));

                });

            }

        });
        $this->products=array_filter($this->products);*/
//echo $link->getLink();
        $content=file_get_contents($link->getLink());
        $crawler =  new Crawler($content);
		
		try{
            $this->name = trim(strip_tags( $crawler->filter('h1')->text()));
        }catch (\Exception $exception){
                    $this->name = "";
        }
		//articul, make, color, country
		
		$crawler->filter('.product-code')->each(function (Crawler $node){
			
			$node->filter('span')->each(function(Crawler $node2){
				
				if($node2->attr('itemprop')=="sku")
					$this->articul=$node2->text();
				
				if($node2->attr('itemprop')=="brand"){
						$this->brand=$node2->text();
						$this->brand=preg_replace("/\s\(.*/","",$this->brand);
					
						$this->country=preg_replace("/.*\(/","",$node2->text());
						$this->country=trim(preg_replace("/\)/","",$this->country));
				}
				if($node2->attr('itemprop')=="value"){
						$this->color=trim(preg_replace("/\,\s?.*/","",$node2->text()));
						$this->description=$node2->text();
				}

			});

        });
		//echo $this->color;
		try{
			if($crawler->filter('.productNotAvailable')->text() =="Товара нет в наличии")
				return NULL;		
		}catch(\Exception $exception){
			
		}

		try{
			$price=$crawler->filter('.product-price')->children()->filter('.price-value')->text();
			$price=preg_replace('/\,\s?.*/',"",$price);
			//$price=preg_replace('/от\s/',"",$price);
			$price=preg_replace('/[^\d]/',"",$price);
			//$price=str_replace('&nbsp;', '', $price);
			//echo $price."=";var_dump($crawler->filter('.price-sales')->children()->evaluate('count(//span)'));
			if( count($crawler->filter('.price-sales')->children()->evaluate('count(//span)')) > 1 ){//price standard and prive drop exist
				$priceDrop=$crawler->filter('.product-price')->children()->filter('.price-standard')->text();
				$priceDrop=preg_replace('/\,\s?.*/',"",$priceDrop);
				//$priceDrop=preg_replace('/от\s/',"",$priceDrop);
				$priceDrop=preg_replace('/[^\d]/',"",$priceDrop);
				//$priceDrop=str_replace("&nbsp;", '', $priceDrop);
				$price=ceil($price+($priceDrop-$price)/2);//echo $price."price";
			}
		}catch(\Exception $exception){
			$price=0;
		}
        //$model = $crawler->filter('meta')-> attr('content');
		$this->sizes=array();
		$crawler->filter('#sizesSet > label')->each(function (Crawler $node2){
					$this->sizes[]=trim($node2->text());
					
        });
		
        try{
		$this->description=$crawler->filter('.product-tab')->html();
		}catch(\Exception $exception){
			
		}
	
	try{
		$this->images=$crawler->filter('.sp-thumbs')->each(function (Crawler $node2){
			return "https://l-shop.ua".$node2->filter('img')->attr('src');
		});
	}catch(\Exception $exception){
		$this->images=NULL;
	}
        //$this->sizes = $this->images = "";//$params[0]['param']

        //build array of products $params[0]['param']
        /*$products = array();
        foreach ($this->products as $product) {
            array_push($params[0]['param'],array('param'=>'Цвет','value'=>$product['color']));
            $products[] = ['name'=>$this->name,'price'=>$this->price,'params'=>$params[0]['param'],'description'=>$description,'sizes'=>$product['sizes'], 'images'=>$product['images'],'link'=>$link->getId(), 'model'=>$model];
            array_pop($params[0]['param']);
        }*/
		if(isset($this->color))
			$params[0]['param'][]=array('param'=>'Цвет','value'=>$this->color);
		if(isset($this->brand))
			$params[0]['param'][]=array('param'=>'Производитель','value'=>$this->brand);
		if(isset($this->country))
			$params[0]['param'][]=array('param'=>'Страна','value'=>$this->country);
	/*echo "<br>".$this->name;
	echo "<br>".$price;
	var_dump($params[0]['param']);
	echo $this->description;
		var_dump($this->sizes);
	echo implode(',',$this->sizes);
	var_dump($this->images);
	echo $this->articul;*/
        return ['name'=>$this->name,'price'=>$price,'params'=>$params[0]['param'],'description'=>$this->description,'sizes'=>implode(',',$this->sizes), 'images'=>$this->images,'link'=>$link->getId(), 'model'=>$this->articul];
        //return $products;

    }
}
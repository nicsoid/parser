<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 21.09.2018
 * Time: 1:20
 */

namespace App\Parser;


use App\Entity\CategoriesToParse;
use App\Entity\LinksToParse;
use App\Parser;

use Symfony\Component\DomCrawler\Crawler;
use Clue\React\Buzz\Browser;
use Psr\Http\Message\ResponseInterface;
use React\EventLoop\Factory;

class ParserStimma implements Parser
{
    private $pages_in_category;
    private $category_products_link;
    private $images;
    private $sizes;
    private $price;

    /**
     * @param $category
     */
    public function getNumberOfPagesInCategory(CategoriesToParse $category)
    {
        $this->pages_in_category = 1;//if there is ony 1 page, <pagination> is not present
        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink().'1')->then(function (ResponseInterface $response) {

            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('.page-numbers')->each(function ( Crawler $node) {
                $this->pages_in_category = ($this->pages_in_category > (int)$node->text()) ? $this->pages_in_category : (int)$node->text();
            });
        });
        $loop->run();

        return $this->pages_in_category;

    }

    public function getCategoryProductLinks(CategoriesToParse $category,$page_to_parse)
    {
        //$crawler =  new Crawler(file_get_contents($category->getCatToParseLink().$page_to_parse));
        //$crawler->filter('.catalog-block')->each(function (Crawler $node, $i) {
        //    $this->category_products_link[]=['info'=>str_ireplace("<br>"," ",$node->filter('.h2')->children()->text()),'link'=>$node->children()->attr("href")];
        //});
        $this->category_products_link=NULL;
        $loop = Factory::create();
        $client = new Browser($loop);
        $client->get($category->getCatToParseLink().$page_to_parse)->then(function (ResponseInterface $response) {

            $crawler =  new Crawler( (string) $response->getBody());
            $crawler->filter('.product-h')->each(function (Crawler $node, $i) {
                if(!($node->children()->filter('span')->text()=='Нет в наличии'))
                $this->category_products_link[]=['info'=>$node->children()->filter('.product-meta')->children()->filter('h2')->text(),'link'=>$node->children()->filter('a')->first()->attr('href')];
            });
        });
        $loop->run();

        return $this->category_products_link;

    }

    public function getProduct(LinksToParse $link, $xml)
    {

        $content=file_get_contents($link->getLink());
        $crawler =  new Crawler($content);

        $prod_name = $crawler->filter('h1')->text();

        preg_match("/(\D+)/s",$prod_name,$name);
        $prod_name=str_ireplace("STIMMA","",$name[0]);

        $vendorCode = $crawler->filter('.sku')->text();
        $model = $vendorCode;

        if($crawler->filter('.woocommerce-product-details__short-description')->count()>0)
            $description = $crawler->filter('.woocommerce-product-details__short-description')->text();
        else
            $description=$prod_name;
        $this->images[] = $crawler->filter('.woocommerce-product-gallery__wrapper')->children()->filter('img')->attr('data-large_image');
        //get parameters
        $params = $crawler->filter('.shop_attributes')->each(function (Crawler $node) {

            $params = $node->filter('tr')->each(function (Crawler $node2) {
                if($node2->children()->eq(0)->text()!="Размер")
                    return array('param'=>$node2->children()->eq(0)->text(),'value'=>trim($node2->children()->eq(1)->text()));
            });
            return array('params'=>$params);

        });
        $params2=NULL;
        foreach ($params[0]['params'] as $param)//removes NULL values parameters
        {
            if($param) $params2[0]['params'][]=$param;
        }
        //end get parameters
        //get sizes
        $this->sizes = $crawler->filter('.attribute-pa_razmer')->children()->eq(1)->children()->each(function (Crawler $node) {
                    return $node->text();
        });
        //end get sizes

        $crawler =  new Crawler($xml);
        $crawler->filter('vendorCode:contains("'.$vendorCode.'")')->siblings()->each(function (Crawler $node) {
            if($node->nodeName()=='cost')
                $this->price=$node->text();
        });

        return ['name'=>$prod_name,'price'=>$this->price,'params'=>$params2[0]['params'],'description'=>$description,'sizes'=>implode(',',$this->sizes), 'images'=>$this->images,'link'=>$link->getId(), 'model'=>$model];

    }
}
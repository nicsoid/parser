<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 23.01.2019
 * Time: 13:28
 */

namespace App\Export;

use App\Entity\CategoriesToParse;
use App\Entity\LinksToParse;
use App\Entity\Product;
use App\Entity\Categories;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;

use Symfony\Component\DomCrawler\Crawler;
use Clue\React\Buzz\Browser;
use Psr\Http\Message\ResponseInterface;
use React\EventLoop\Factory;

class ExportPrestashop
{
    /* Visibility - both,catalog,search,none */
    public $exportFields=[
        0=>["Product ID",0],
        1=>["Active (0/1)",0],
        2=>["Name",1],
        3=>["Categories (x,y,z...)",0],
        4=>["Price tax included",0],
        5=>["Tax rules ID",0],
        6=>["Wholesale price",0],
        7=>["On sale (0/1)",0],
        8=>["Discount amount",0],
        9=>["Discount percent",0],
        10=>["Discount from (yyyy-mm-dd)",0],
        11=>["Discount to (yyyy-mm-dd)",0],
        12=>["Reference #",0],
        13=>["Supplier reference #",0],
        14=>["Supplier",0],
        15=>["Manufacturer",0],
        16=>["EAN13",0],
        17=>["UPC",0],
        18=>["Ecotax",0],
        19=>["Width",0],
        20=>["Height",0],
        21=>["Depth",0],
        22=>["Weight",0],
        23=>["Quantity",0],
        24=>["Minimal quantity",0],
        25=>["Low stock level",0],
        26=>["Visibility",0],
        27=>["Additional shipping cost",0],
        28=>["Unity",0],
        29=>["Unit price",0],
        30=>["Short description",0],
        31=>["Description",0],
        32=>["Tags (x,y,z...)",0],
        33=>["Meta title",0],
        34=>["Meta keywords",0],
        35=>["Meta description",0],
        36=>["URL rewritten",0],
        37=>["Text when in stock",0],
        38=>["Text when backorder allowed",0],
        39=>["Available for order (0 = No, 1 = Yes)",0],
        40=>["Product available date",0],
        41=>["Product creation date",0],
        42=>["Show price (0 = No, 1 = Yes)",0],
        43=>["Image URLs (x,y,z...)",0],
        44=>["Image alt texts (x,y,z...)",0],
        45=>["Delete existing images (0 = No, 1 = Yes)",0],
        46=>["Feature(Name:Value:Position)",0],
        47=>["Available online only (0 = No, 1 = Yes)",0],
        48=>["Condition",0],
        49=>["Customizable (0 = No, 1 = Yes)",0],
        50=>["Uploadable files (0 = No, 1 = Yes)",0],
        51=>["Text fields (0 = No, 1 = Yes)",0],
        52=>["Out of stock",0],
        53=>["ID / Name of shop",0],
        54=>["Advanced stock management",0],
        55=>["Depends On Stock",0],
        56=>["Warehouse",0]
    ];

    public $exportFieldsMandatory=[0,1,2,3,4,7,8,9,10,11,16,23,26,30,31,32,33,34,35,36,42,43,44,45,46,48];

}
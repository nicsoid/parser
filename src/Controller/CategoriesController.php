<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use App\Entity\Categories;

/**
 * Require ROLE_USER for *every* controller method in this class.
 *
 * @IsGranted("ROLE_USER")
 */
class CategoriesController extends AbstractController
{
    private $categories;

    /**
     * @Route("/categories", name="categories")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        //get all categories
        $this->categories = $this->getDoctrine()->getRepository(Categories::class)
            ->findAll();

        $responseCategory["Main"] = "0";
        foreach ($this->categories as $category){

            $responseCategory[$category->getName()] = $category->getId();
         }

        //create new category
        $form = $this->createFormBuilder()
            ->add('name',TextType::class,array(
                'label' => 'Category Name',
            ))
            ->add('parent', ChoiceType::class, array(

                'choices' => $responseCategory,
            ))
            ->add('submit', SubmitType::class, array('label' => 'Add Category'))
            ->getForm();


        $form->handleRequest($request);
        if($form->isSubmitted()){

            $newCategory = new Categories();
            $data = $form->getData();
            $newCategory->setName($data['name']);
            $newCategory->setParent($data['parent']);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newCategory);
            $entityManager->flush();

        }

        //get all categories
        $this->categories = $this->getDoctrine()->getRepository(Categories::class)
            ->findAll();

        return $this->render('categories/index.html.twig', [
            'controller_name' => 'CategoriesController',
            'categories' => $this->categories,
            'form' => $form->createView(),

        ]);
    }

    /**
     * @Route("/categories-edit-{id}", name="categories-edit", requirements={"id"="\d+"})
     * @param Request $request
     * @return Response
     */
    public function edit($id, Request $request)
    {
        //get category
        $category = $this->getDoctrine()->getRepository(Categories::class)
            ->find($id);

        //get all categories
        $this->categories = $this->getDoctrine()->getRepository(Categories::class)
            ->findAll();

        $defaults = array(
            'name'=>$category->getName(),
            'cat_parent'=>$category->getParent(),
            'promua_id'=>$category->getPromua(),
            'Keywords'=>$category->getKeywords(),
            'meta_description'=>$category->getMetaDescription(),
            'params'=>$category->getParams()
        );

        $form = $this->createFormBuilder($defaults)
            ->add('name',TextType::class, array('label'=>'Name'))
            ->add('cat_parent',TextType::class, array('label'=>'Parent Category'))
            ->add('promua_id',TextType::class, array('label'=>'Prom Id'))
            ->add('Keywords', TextareaType::class, array('label'=>'Keywords - обязательно','attr' => array('columns' => '20','rows'=>'2','class'=>'form-control-sm')))
            ->add('meta_description', TextareaType::class, array('label'=>'Meta Описание - желательно','attr' => array('columns' => '20','rows'=>'2','class'=>'form-control-sm')))
            ->add('params', TextareaType::class, array('label'=>'Parameters List','attr' => array('columns' => '20','rows'=>'10','class'=>'form-control-sm')))
            ->add('submit', SubmitType::class, array('label' => 'Update Category'))
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted()) {
            $data = $form->getData();
            $category->setName($data['name']);
            $category->setParent($data['cat_parent']);
            $category->setPromua($data['promua_id']);
            $category->setKeywords($data['Keywords']);
            $category->setMetaDescription($data['meta_description']);
            $category->setParams($data['params']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category);
            $entityManager->flush();


        }

        return $this->render('categories/edit.html.twig', [
            'controller_name' => 'CategoriesController',
            'category' => $category,
            'form' => $form->createView(),
        ]);
        }


    /**
     * @Route("/categories-delete-{id}", name="categories-delete", requirements={"id"="\d+"})
     * @return Response
     */
    public function delete($id)
    {

        $entityManager = $this->getDoctrine()->getManager();
        $category = $entityManager->getRepository(Categories::class)->find($id);
        $entityManager->remove($category);
        $entityManager->flush();

        return $this->redirectToRoute('categories');

    }
}

<?php

namespace App\Controller;


use App\Entity\Product;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use App\Entity\CategoriesToParse;
use App\Entity\Forms;
use App\Entity\LinksToParse;
use App\Entity\Programs;
use Throwable;

/**
 * Require ROLE_USER for *every* controller method in this class.
 *
 * @IsGranted("ROLE_USER")
 */
class ParserController extends AbstractController
{
    private $pages_to_process = 0;
    private $products_to_parse = 5;
    private $cats_to_parse;
    private $categoryToParse;
    private $category_products_link = [];
    private $cat_to_parse_form;
    private $numpages;//number of pages of category to parse
    private $links;
    private $products;
    private $images_to_copy = 6;

    /**
     * @Route("/parser", name="parser")
     */
    public function index()
    {
        //list all categories to parse
        $this->cats_to_parse = $this->getDoctrine()->getRepository(CategoriesToParse::class)
            ->findAllCategories();

        return $this->render('parser/index.html.twig', [
            'controller_name' => 'ParserController',
            'cats_to_parse' => $this->cats_to_parse,
        ]);
    }

    /**
     * @Route("/parser-indexer-pages", name="indexer-pages")
     * @param Request $request
     * @return JsonResponse
     */
    public function parseCategoryForPages(Request $request)
    {
        $keep = null;
        $category = $request->get('id');
        //$category=1;
        if ($category) {

            try {
                //get number of pages of category
                $this->categoryToParse = $this->getDoctrine()->getRepository(CategoriesToParse::class)
                    ->find($category);
                $this->cat_to_parse_form = $this->getDoctrine()->getRepository(Forms::class)
                    ->findOneBy(array('id' => $this->categoryToParse->getFormId()));
                if ($this->cat_to_parse_form)//if form exists, get number of pages to parse in category in order to get links to products to parse
                {
                    $p1 = 'App\Parser\\' . $this->cat_to_parse_form->getCatTemplate();
                    $parser = new $p1();
                    $this->pages_to_process = $parser->getNumberOfPagesInCategory($this->categoryToParse);

                }
                //get products link from all pages

            } catch (Throwable $t) {
                return new JsonResponse(['data' => null, 'category' => $category, 'formId' => 'error', 'numpages' => "error"]);
            }

            return new JsonResponse(['data' => $request->get('id'), 'category' => $category, 'numpages' => $this->pages_to_process]);
        }
        //return new JsonResponse(['data' => null, 'category' => $category, 'formId' => 'error', 'numpages' => $request->get('id')]);
    }

    /**
     * @Route("/parser-indexer-links", name="parser-indexer-links")
     * @param Request $request
     * @return JsonResponse
     */
    public function parseCategoryForProducts(Request $request)
    {

        //input: categoryId, total number of pages.
        //output: total numpages, number of pages parsed
        //parse one page at once
        if ($request->get('id')) {

            $result = null;
            $this->categoryToParse = $this->getDoctrine()->getRepository(CategoriesToParse::class)
                ->find($request->get('id'));
            $this->cat_to_parse_form = $this->getDoctrine()->getRepository(Forms::class)
                ->findOneBy(array('id' => $this->categoryToParse->getFormId()));

            $numpages = $request->get('numpages');
            $page_to_parse = $request->get('page_to_parse');

            //check if this is first request for parsing, update all links from this cat to status 10, to separate old links later
            if($page_to_parse==1){//set all status to 10
                $this->getDoctrine()->getRepository(Product::class)->setStatusByCategoryToParse($this->categoryToParse->getId(),10);
            }

            if ($page_to_parse <= $numpages) {
                $result = $this->productLinksFromCategory($numpages, $page_to_parse);
            }

            return new JsonResponse(['data' => 1, 'category' => $this->categoryToParse->getId(), 'numpages' => $numpages, 'page_to_parse' => $page_to_parse, 'result' => $result[0], 'toupdate' => $result[1]]);
        }

    }

    /**
     * @Route("/parser-indexer-products", name="indexer-products")
     * @param Request $request
     * @return JsonResponse
     */
    public function parseProducts(Request $request)
    {
        $proceed = false;
        $product_counter = 0;

        $category = $request->get('id');

        //set all products status to 10
        if($request->get('first_request'))
            $this->getDoctrine()->getRepository(Product::class)->setProductStatusByCategoryToParse($category,10);

        if ($request->get('id')) {
            //category to parse products from
            $this->categoryToParse = $this->getDoctrine()->getRepository(CategoriesToParse::class)
                ->find($category);
            //form for this category - code to run to extract product's data
            $this->cat_to_parse_form = $this->getDoctrine()->getRepository(Forms::class)
                ->findOneBy(array('id' => $this->categoryToParse->getFormId()));
            //product links to parse
            $this->links = $this->getDoctrine()->getRepository(LinksToParse::class)
                ->findBy(['category_to_parse' => $this->categoryToParse->getId(), 'status' => 0], ['id' => 'ASC'], $this->products_to_parse);

            if ($this->links) {
                $xml_content = "";
                if ($this->categoryToParse->getUseXml()) {//get XML file if needed
                    $program = $this->getDoctrine()->getRepository(Programs::class)
                        ->find($this->categoryToParse->getProgram());
				
					$xml_content = $program->getXmlFileContent();
					/*try{
                    	$xml_content = file_get_contents($program->getXmlFile());
					}catch(\Exception $e){
						sleep(2);
						$xml_content = file_get_contents($program->getXmlFile());
						
					}*/
				
				}
                //go through links
                foreach ($this->links as $link) {
                    $p1 = 'App\Parser\\' . $this->cat_to_parse_form->getCatTemplate();
                    $parser = new $p1();
                    //get product
                    $item2 = $parser->getProduct($link, $xml_content); //if result is only one product or many products
                    if(isset($item2['name']))//put it into an array
                        $item3[0]=$item2;
                    else $item3=$item2;//or leave it - array of products returned by getProduct
                    //insert products to DB if product was returned
                    if($item2 != NULL)
                    foreach ($item3 as $item) {

                        //if we are processing array of products from one link - check by link and images,
                        //otherwise if we have 1 link for 1 product - check by link.
                        if(isset($item2['name'])) {//one product
                            $product_from_db = $this->getDoctrine()->getRepository(Product::class)->findOneBy(['link'=>$item['link']]);
                        }else{//few products
                            $product_from_db = $this->getDoctrine()->getRepository(Product::class)->findOneBy(['link'=>$item['link'],'images'=>json_encode($item['images'])]);
                        }
                        //if product is in database - update, if not - add.
                        if(!$product_from_db) {//product is not in DB
                            $product = new Product();
                            $product->setName($item['name']);
                            $product->setDescription($item['description']);
                            $product->setCatMain($this->categoryToParse->getCatMainId());
                            $product->setCatParsed($this->categoryToParse->getId());
                            $product->setDateParsed(new \DateTime('now'));
                            $product->setLink($item['link']);
                            $product->setParams(json_encode($item['params']));
                            $product->setSizes($item['sizes']);
                            $product->setImages(json_encode($item['images']));
                            $product->setPrice($item['price']);
                            $product->setStatus(0);
                            $product->setModel($item['model']);

                            $entityManager = $this->getDoctrine()->getManager();
                            $entityManager->persist($product);
                            $entityManager->flush();
                        }else{//product in DB - update
                            $entityManager = $this->getDoctrine()->getManager();
                            $product = $entityManager->getRepository(Product::class)->find($product_from_db->getId());
                            $product->setDescription($item['description']);
                            $product->setDateParsed(new \DateTime('now'));
                            $product->setParams(json_encode($item['params']));
                            $product->setSizes($item['sizes']);
                            $product->setImages(json_encode($item['images']));
                            $product->setImagesTimodna($product->getImagesTimodna());
                            $product->setPrice($item['price']);
                            $product->setStatus(0);

                            $entityManager->flush();

                        }
                        //update link status
                        $link->setStatus(1);
                        $link->setDateParsed(new \DateTime('now'));
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($link);
                        $entityManager->flush();

                        $product = NULL;
                        $product_counter++;
                    }else{//product is not available
					
						//update link status
                        $link->setStatus(1);
                        $link->setDateParsed(new \DateTime('now'));
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($link);
                        $entityManager->flush();

                        $product = NULL;
                        $product_counter++;
						
					}

                }
                //var_dump($products);
                //end insert products to DB
            }


            $this->links = $this->getDoctrine()->getRepository(LinksToParse::class)
                ->findBy(['category_to_parse' => $this->categoryToParse->getId(), 'status' => 0], ['id' => 'ASC']);

            if (count($this->links)>0)
                $proceed = true;

            //check for loops...
            if($product_counter==0 && count($this->links)==1)
                $proceed = false;

            return new JsonResponse(['proceed' => $proceed, 'id' => $this->categoryToParse->getId(), 'numproducts' => $product_counter, 'products_left'=>count($this->links)]);
        }

        return new JsonResponse(['proceed' => $proceed, 'id' => 0, 'numproducts' => 0, 'products_left'=>0]);
    }

    /**
     * @Route("/p2", name="p2")
     * @param $totalPages
     * @param $page_to_parse
     * @return int
     */
    public function productLinksFromCategory($totalPages = 3, $page_to_parse = 1)
    {

        if (!$this->categoryToParse) {
            $this->categoryToParse = $this->getDoctrine()->getRepository(CategoriesToParse::class)
                ->find(4);
            $this->cat_to_parse_form = $this->getDoctrine()->getRepository(Forms::class)
                ->findOneBy(array('id' => $this->categoryToParse->getFormId()));
        }
        //$loop = Factory::create();
        //$client = new Browser($loop);
        $p1 = 'App\Parser\\' . $this->cat_to_parse_form->getCatTemplate();
        $parser = new $p1();
        $this->category_products_link = $parser->getCategoryProductLinks($this->categoryToParse, $page_to_parse);

        $links = 0;
        $toupdate = 0;
        //$entityManager = $this->getDoctrine()->getManager();
        //insert links into DB
        if($this->category_products_link)
        foreach ($this->category_products_link as $link) {


                $in_db = $this->getDoctrine()->getRepository(LinksToParse::class)->findBy(['link' => $link['link']]);
                //$in_db = $entityManager->getRepository(LinksToParse::class)->findBy(['link' => $link['link']]);

            if (!$this->getDoctrine()->getRepository(LinksToParse::class)->findBy(['link' => $link['link']])) {//new link
                $newLinkToParse = new LinksToParse();
                $newLinkToParse->setDateParsed(new \DateTime('now'));
                $newLinkToParse->setCatMainId($this->categoryToParse->getCatMainId());
                $newLinkToParse->setCategoryToParse($this->categoryToParse->getId());
                $newLinkToParse->setFormId($this->categoryToParse->getFormId());
                $newLinkToParse->setInfo($link['info']);
                $newLinkToParse->setLink($link['link']);
                $newLinkToParse->setStatus(0);
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($newLinkToParse);
                $entityManager->flush();
                $links++;
            }else{

                $entityManager = $this->getDoctrine()->getManager();
                $product = $entityManager->getRepository(LinksToParse::class)->find($in_db[0]->getId());
                $product->setDateParsed(new \DateTime('now'));
                //$entityManager = $this->getDoctrine()->getManager();
                $product->setInfo($link['info']);
                $product->setStatus(0);
                $entityManager->flush();
                $toupdate++;
            }

        }

        return array($links,$toupdate);
        //return new JsonResponse(['data'=>$this->category_products_link, 'link'=>$this->categoryToParse->getCatToParseLink().$page_to_parse]);
    }

    /**
     * @Route("/parser-images", name="indexer-images")
     * @param Request $request
     * @return JsonResponse
     */
    public function grabImages(Request $request)
    {
        $proceed = false;
        if ($request->get('id')) {
            //get products
            $products = $this->getDoctrine()->getRepository(Product::class)
                ->findBy(['cat_parsed' => $request->get('id'), 'status' => 0], ['id' => 'ASC'], $this->images_to_copy);

            if ($products) {
                foreach ($products as $product) {
                    $images = json_decode($product->getImages());
                    if ($images) {//update product
                        $images_new = array();

                        if($product->getImagesTimodna() == NULL)//check if we have images already, check there is some json string already
                        {
                            foreach ($images as $item)
                                $images_new[] = $this->copyImage($product->getName(), $item, count($images_new), $product->getId());
                            $product->setImagesTimodna(json_encode($images_new));
                        }
                        //$product->setImagesTimodna(json_encode($images_new));
                        $product->setStatus(1);
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($product);
                        $entityManager->flush();
                        $images_new = NULL;
                    }else{ //product has no images - set status=10
						$product->setStatus(10);
                        $entityManager = $this->getDoctrine()->getManager();
                        $entityManager->persist($product);
                        $entityManager->flush();
					}
                }
                //$proceed = true;
            }

            $products = $this->getDoctrine()->getRepository(Product::class)
                ->findBy(['cat_parsed' => $request->get('id'), 'status' => 0], ['id' => 'ASC']);
			
			if(count($products)>0)
				$proceed = true;

            return new JsonResponse(['proceed' => $proceed, 'id' => $request->get('id'), 'products_left' => count($products)]);
        }

        return new JsonResponse(['proceed' => $proceed, 'id' => 'no id', 'products_left' => 0]);
    }

     /**
     *
     *
     */
    public function copyImage($name, $link, $iteration,$productId=0)
    {

		$local = "/var/www/uamedia/data/www/timodna.com.ua/www/img/pp/";
        //$local = "/home/ae000/timodna.com.ua/www/img/pp/";
        //$local = "e:\\cites\\parser\\img\\";
        $name = str_ireplace(" ", "_", $this->transliterate($name)).(rand(10,99)).$productId. ".jpg";
        $fileSystem = new Filesystem();
		try{
        $fileSystem->copy($link, $local . $iteration . "_" . $name);
		}catch(\Exception $e){
			sleep(3);
			try{
			$fileSystem->copy($link, $local . $iteration . "_" . $name);
			}catch(\Exception $e){
			sleep(10);
				try{
				$fileSystem->copy($link, $local . $iteration . "_" . $name);
				}catch(\Exception $e){
					#TODO return null value
					return "";
				}
			}
		}
        return $iteration . "_" . $name;
    }

    /**
     *   transliterate
     * @param string $string
     * @return string
     */

    public function transliterate($textcyr = null, $textlat = null)
    {
        $cyr = array(
            'ж', 'ч', 'щ', 'ш', 'ю', 'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ь', 'я', 'ё', 'э', 'ы',
            'Ж', 'Ч', 'Щ', 'Ш', 'Ю', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ъ', 'Ь', 'Я', 'Ё', 'Э', 'Ы');
        $lat = array(
            'zh', 'ch', 'shch', 'sh', 'yu', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'y', '', 'ya', 'io', 'e', 'i',
            'Zh', 'Ch', 'Shch', 'Sh', 'Yu', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'c', 'Y', 'X', 'Q', 'IO', 'E', 'I');
        if ($textcyr) return str_replace($cyr, $lat, $textcyr);
        else if ($textlat) return str_replace($lat, $cyr, $textlat);
        else return null;
    }

    /**
     * Copy XML file to local dir
     *
     */
    public function copyXMLFile($link_to_file){

        copy($link_to_file,'/../'.dirname(__FILE__));

    }

    /**
     * @Route("/parser-run-all", name="automatic-parser")
     * @param Request $request
     * @return JsonResponse
     */
    public function parseAutomatic(Request $request){

        //get categories and stage
        /* categories, currentCategory, stage, params */
        $categories = $request->get('categories');
        $currentCategory = $request->get('id');
        $stage = $request->get('stage');
        $params = explode(",",$request->get('params'));

        switch ($stage){
            case 1: //parseCategoryForPages
                $json_numpages = $this->parseCategoryForPages($request);
                $json_numpages_array = json_decode($json_numpages->getContent(),true);
                $params[0] = $json_numpages_array["numpages"];
                $params[1] = 1;//set page to parse 1 - begin parsing pages

                $categories = explode(",",$categories);
                //array_shift($categories);
                $currentCategory = $request->get('id');
                $stage = 2;
                //pop first element from the array and set stage 2;
                break;
            case 2: //parseCategoryForProducts
                $request->request->set('numpages',$params[0]);
                $request->request->set('page_to_parse',$params[1]);

                $json_parsed_links = $this->parseCategoryForProducts($request);
                $parsed_links_array = json_decode($json_parsed_links->getContent(),true);//var_dump($parsed_links_array);
                $params[0] = $params[0];
                $params[1] = $params[1]+1;

                $stage = $params[0] >= $params[1] ? 2 : 3;
                if($stage==3) {
                    $params[1]=1;
                    $params[2]=1;//it is first request to parse products after this one
                }//reset counter for page number to parse
                $categories = explode(",",$categories);
                $currentCategory = $request->get('id');

                break;
            case 3: //parseProducts.  we need first_request key to set status of products to 10 - to be able to separate not active products

                $request->request->set('first_request',$params[2]);

                $json_parsed_products = $this->parseProducts($request);
                $parsed_products_array = json_decode($json_parsed_products->getContent(),true);

                if($parsed_products_array["proceed"])
                {
                    $stage = 3;
                    $params[3] = $parsed_products_array['numproducts'];
                    $params[4] = $parsed_products_array['products_left'];
                }else{
                    $stage = 4;
                    $params[3] = 0;
                    $params[4] = 0;
                    //$categories = explode(",",$categories);
                }
                $categories = explode(",",$categories);
                $currentCategory = $request->get('id');

                $params[2] = 0;//set it will be not first request to parse products

                break;
            case 4: //grabImages

                //$this->images_to_copy=5;

                $json_parsed_images = $this->grabImages($request);
                $parsed_images_array = json_decode($json_parsed_images->getContent(),true);
                $params[5]=$parsed_images_array['products_left'];

                if($parsed_images_array['proceed'])
                {
                    $stage = 4;
                    $categories = explode(",",$categories);
                    $currentCategory = $request->get('id');
                }else{//

                    $categories = explode(",",$categories);
                    array_shift($categories);

                    if(count($categories)>0)
                    {
                        $currentCategory = $categories[0];
                        $stage = 1;
                    }else{
                        $stage = 0;
                        $currentCategory=0;
                    }
                    $params[5] = 0;
                }

                break;
            default:

                break;
        }

        //stats
        //$file = fopen('f1.txt','a');
        //fwrite($file, date("H:i:s").'; cat: '.$request->get('id').'; stage: '.$stage.'; params: '.implode(",",$params)."\n");
        //fclose($file);

        //send back list of categories, stage, current category, params of stage
        return new JsonResponse(['categories' => $categories, 'currentCategory' => $currentCategory, 'stage' => $stage, 'params'=>$params]);

    }


}
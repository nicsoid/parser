<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\LinksToParse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
  * Require ROLE_USER for *every* controller method in this class.
  *
  * @IsGranted("ROLE_USER")
  */
class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function index(Request $request)
    {
        //
        $products=array();
        if($request->get('search'))//find product
        {
            $products = $this->getDoctrine()->getRepository(Product::class)->findProductsByArticul($request->get('search'));

        }

        return $this->render('index.html.twig', [
            'controller_name' => 'IndexController',
            'products'=>$products
        ]);
    }

}

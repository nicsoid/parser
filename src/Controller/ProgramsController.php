<?php

namespace App\Controller;

//use Doctrine\DBAL\Types\TextType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use App\Entity\Programs;

/**
 * Require ROLE_USER for *every* controller method in this class.
 *
 * @IsGranted("ROLE_USER")
 */
class ProgramsController extends AbstractController
{
    private $programs;

    /**
     * @Route("/programs", name="programs")
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        //add new program

        $form = $this->createFormBuilder()
            ->add('name',TextType::class,array(
                'label' => 'Program Name',
            ))
            ->add('website', TextType::class,array(
                'label' => 'Website Link',
            ))  
            ->add('xmllink', TextType::class,array(
                'label' => 'XML file',
                'required'=>false
            ))
            ->add('submit', SubmitType::class, array('label' => 'Add Program'))
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted()){

            $newProgram = new Programs();
            $data = $form->getData();
            $newProgram->setName($data['name']);
            $newProgram->setDateAdded(new \DateTime('now'));
            $newProgram->setProductsCount(0);
            $newProgram->setWebsite($data['website']);
            $newProgram->setXmlFile($data['xmllink']);
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newProgram);
            $entityManager->flush();

        }

        //get list of programs
        $this->programs = $this->getDoctrine()->getRepository(Programs::class)
            ->findAll();

        return $this->render('programs/index.html.twig', [
            'controller_name' => 'ProgramsController', 'form' => $form->createView(), 'programs'=>$this->programs
        ]);
    }
}

<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Categories;
use App\Entity\CategoriesToParse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Export;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ExportController extends AbstractController
{
    private $prestaWebServicesKey = 'EXLL7821I1DIXMCBIYP2B16WHE5YM6QM';
    private $prestaWebServicesProdUrl = 'http://www.timodna.com.ua';

    /**
     * @Route("/export", name="export")
     */
    public function index()
    {


        return $this->render('export/index.html.twig', [
            'controller_name' => 'ExportController',
        ]);
    }
    /**
     * @Route("/export-categories", name="export-categories")
     */
    public function ExportCategories()
    {

        $d = ";";
        //get list of our categories
        $categories = $this->getDoctrine()->getRepository(Categories::class)->findAll();

        $header = "Category ID;Active (0/1);Name;Parent category;Root category (0/1);Description;Meta title;Meta keywords;Meta description;URL rewritten\n";

        $s = "";
        foreach($categories as $category){
            if($category->getParent()!=0)
            {
                $parent=$this->getDoctrine()->getRepository(Categories::class)->findParent($category->getParent());
                $parent_name=$parent->getName();
                $root=0;
				$parent_id=$category->getParent()+5;
            }else
            {
                $parent_name="Главная";
                $root=1;
				$parent_id=2;
            }
            $url=$this->transliterate(str_ireplace(",","",$parent_name."-".$category->getName()));
            $s .=($category->getId()+5).$d."1".$d.$category->getName().$d.$parent_id.$d.$root.$d.$category->getName().$d.$category->getName().$d.$category->getName().$d.$category->getName().$d.$url."\n";

        }

        file_put_contents("export_cats.csv",$header.$s);
        file_put_contents("/var/www/uamedia/data/www/timodna.com.ua/www/admin2807/import/export_cats.csv",$header.$s);

        return new JsonResponse(['link' => "export_cats.csv"]);
    }

    /**
     * @Route("/export-products", name="export-products")
     */
    public function ExportProducts()
    {
        $header="";
        $s="";
        $d=";";
        $prods_in_file = 1500;
        $export=new Export\ExportPrestashop();

        //make header
        foreach ($export->exportFieldsMandatory as $item) {
            $header .= $export->exportFields[$item][0].";";
        }
        $header .="\n";

        //get all articuls
        //$articuls=$this->getDoctrine()->getRepository(Product::class)->findArticuls();

        //var_dump($articuls);

        //go through articuls and build products, then build assosiacions

        $parameters_mapping = $this->getDoctrine()->getRepository(CategoriesToParse::class)->getParametersMapping();
        //get markups
        $markups = $this->getDoctrine()->getRepository(CategoriesToParse::class)->getMarkups();

        //get categories
        $categories = $this->getDoctrine()->getRepository(Categories::class)
            ->findAll();
        $categories_keywords=[];
        foreach($categories as $category) {

            $categories_keywords[$category->getId()]=[$category->getKeywords(),$category->getMetaDescription()];

        }

        $products = $this->getDoctrine()->getRepository(Product::class)->
        findForTiModnaComUa();
        $p=0;
        $f=0;
        foreach ($products as $product)
        {

            //apply our names for parameters
            $parameters_map = $parameters_mapping[$product["cat_parsed"]];
            $params = json_decode($product["params"]);
            $params_updated = [];
            if ($params) {
                foreach ($params as $param1) {
                    if (isset($parameters_map[trim($param1->param)]))
                        $tmp_param_name = strlen(trim($parameters_map[trim($param1->param)])) > 1 ? trim($parameters_map[trim($param1->param)]) : $param1->param;
                    else
                        $tmp_param_name = $param1->param;
                    $tmp_param_value = $param1->value;
                    $params_updated[]=array('param'=>$tmp_param_name,'value'=>$param1->value);
                    if($tmp_param_name=='Цвет')
                        $product['color'] = $param1->value;
                }

            }

            $product['sizes'] = explode(',',$product['sizes']);
            $product["pricenew"] = round($product["price"] * (1+$markups[$product["cat_parsed"]]/100), -1);
            $product["priceopt"] = round($product["price"] * (1+$markups[$product["cat_parsed"]]/100-0.1), -1);



            $s .=$product['id'].$d;     //0
            $s .="1".$d;                //1
            $s .=$product['name'].$d;   //2
            $s .=$product['category'].$d;//3
            $s .=$product['pricenew'].$d;//4
            $s .="0".$d;                //7
            $s .="0".$d;                //8
            $s .="0".$d;                //9
            $s .="0".$d;                //10
            $s .="0".$d;                //11
            $s .=$product['articul'].$d;                //16
            $s .="5".$d;                //23
            $s .="both".$d;                //26
            //$s .=substr(strip_tags(preg_replace('/\s+/', ' ', $product['description'])),0,200).$d;                //30
            //$s .=strip_tags(preg_replace('/\s+/', ' ', substr($product['description'],1,200))).$d;                //30
            $s .=strip_tags(str_ireplace(';','.',preg_replace('/\s+/', ' ', mb_substr($product['description'],0,200)))).$d;                //30
            $s .=strip_tags(str_ireplace(';','.',preg_replace('/\s+/', ' ', $product['description']))).$d;                //31
            $s .=$this->makeMetaKeywords($product,$categories_keywords[$product['cat_main']]).$d;                //32
            $s .=$product['name'].$d;                //33
            $s .=$this->makeMetaKeywords($product,$categories_keywords[$product['cat_main']]).$d;                //34
            $s .=$this->makeMetaDescription($product,$categories_keywords[$product['cat_main']]).$d;                //35
            $s .="".$d;                 //36
            $s .="1".$d;                //42

            //images 43
            $images = json_decode($product["images"]);
            $img_alts = "";
            $img_links ="";
            if ($images) {
                foreach ($images as $image) {
                    $img_links .="http://www.timodna.com.ua/img/pp/".$image.",";
                    $img_alts .= $product['name'].",";
                }
            }
            $s .=rtrim($img_links,',').$d;//close 43 - images
            $s .=rtrim($img_alts,',').$d;                //44
            $s .="1".$d;                //45


            //sizes
            $sizes_features = "";
            $i=1;
            foreach ($product["sizes"] as $sizes) {
                $sizes_features .= "Размер:$sizes:$i,";
                $i++;
            }
            $sizes_features=rtrim($sizes_features,',');
            //features

            //get all features
            $params_text="";
            foreach ($params_updated as $param)
            {
                $params_text .=$param['param'].":".str_replace(array(',',';'),' ',$param['value']).":1";
                $params_text .=",";
            }
            $params_text = rtrim($params_text,',');


            $s .=$sizes_features.",".$params_text.$d;                //46

            $s .="\n";
            $p++;

            if($p>=$prods_in_file)
            {
                file_put_contents("export_prods$f.csv",$header.$s);
                file_put_contents("/var/www/uamedia/data/www/timodna.com.ua/www/admin2807/import/export_prods$f.csv",$header.$s);
                $s="";
                $p=0;
                $f++;
            }

        }
        if($s!="")
        {
            file_put_contents("export_prods$f.csv",$header.$s);
            file_put_contents("/var/www/uamedia/data/www/timodna.com.ua/www/admin2807/import/export_prods$f.csv",$header.$s);
        }

        //file_put_contents("export_prods.csv",$header.$s);
        //file_put_contents("/home/ae000/timodna.com.ua/www/admin2807/import/export_prods.csv",$header.$s);

        return new JsonResponse(['link' => "export_prods.csv"]);
    }

    /**
     *   transliterate
     * @param string $string
     * @return string
     */

    public function transliterate($textcyr = null, $textlat = null)
    {   $textcyr = str_replace(" ","-",$textcyr);
        $cyr = array(
            'ж', 'ч', 'щ', 'ш', 'ю', 'а', 'б', 'в', 'г', 'д', 'е', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ъ', 'ь', 'я', 'ё', 'э', 'ы',
            'Ж', 'Ч', 'Щ', 'Ш', 'Ю', 'А', 'Б', 'В', 'Г', 'Д', 'Е', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ъ', 'Ь', 'Я', 'Ё', 'Э', 'Ы');
        $lat = array(
            'zh', 'ch', 'shch', 'sh', 'yu', 'a', 'b', 'v', 'g', 'd', 'e', 'z', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'y', '', 'ya', 'io', 'e', 'i',
            'Zh', 'Ch', 'Shch', 'Sh', 'Yu', 'A', 'B', 'V', 'G', 'D', 'E', 'Z', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'F', 'H', 'c', 'Y', 'X', 'Q', 'IO', 'E', 'I');
        if ($textcyr) return str_replace($cyr, $lat, $textcyr);
        else if ($textlat) return str_replace($lat, $cyr, $textlat);
        else return null;
    }

    /** returns keywords for product. create keywords when exporting, to make for separate sizes,colors.
     *
     * @return string
     */
    public function makeMetaKeywords($product, $categoryKeywords=NULL,$size="") : string
    {
        $product['color'] = isset($product['color']) ? $product['color'] : "";

        if(isset($categoryKeywords)) {
            $keywords = str_ireplace("{name}", $product['name'], $categoryKeywords[0]);
            $keywords = str_ireplace("{size}", $size, $keywords);
            $keywords = str_ireplace("{color}", $product['color'], $keywords);
            $keywords = str_ireplace("{price}", $product['pricenew'], $keywords);


        }else{
            try {
                $keywords = $product['name'] . ' ' . $size . ' ' . $product['color'] . ' ' . $product['pricenew'];
            }catch (\Exception $e)
            {
                $keywords = $product['name'].' '.$product['pricenew'];
            }
        }
        return $keywords;

    }

    /** returns meta desc for product. create desc when exporting, to make for separate sizes,colors.
     *
     * @return string
     */
    public function makeMetaDescription($product, $categoryKeywords=NULL,$size="") : string
    {
        $product['color'] = isset($product['color']) ? $product['color'] : "";
        $description = str_ireplace("{name}",$product['name'],$categoryKeywords[1]);
        $description = str_ireplace("{size}",$size,$description);
        $description = str_ireplace("{color}",$product['color'],$description);
        $description = str_ireplace("{price}",$product['pricenew'],$description);

        return html_entity_decode($description);

    }


    /**
     *
     * @Route("/export-products-api", name="export-products-api")
     */
    public function PrestashopApiProducts(){

        $ps = new \PrestaShopWebservice($this->prestaWebServicesProdUrl, $this->prestaWebServicesKey, true);

        $opt['resource'] = 'products';
        $opt['id'] = '19';
        $xml = $ps->get($opt);
        var_dump($xml);
        $header="";
        $s="";
        $d=";";
        $prods_in_file = 50;
        $export=new Export\ExportPrestashop();

        //make header
        foreach ($export->exportFieldsMandatory as $item) {
            $header .= $export->exportFields[$item][0].";";
        }
        $header .="\n";

        //get all articuls
        //$articuls=$this->getDoctrine()->getRepository(Product::class)->findArticuls();

        //var_dump($articuls);

        //go through articuls and build products, then build assosiacions

        $parameters_mapping = $this->getDoctrine()->getRepository(CategoriesToParse::class)->getParametersMapping();
        //get markups
        $markups = $this->getDoctrine()->getRepository(CategoriesToParse::class)->getMarkups();

        //get categories
        $categories = $this->getDoctrine()->getRepository(Categories::class)
            ->findAll();
        $categories_keywords=[];
        foreach($categories as $category) {

            $categories_keywords[$category->getId()]=[$category->getKeywords(),$category->getMetaDescription()];

        }

        $products = $this->getDoctrine()->getRepository(Product::class)->
        findForTiModnaComUa();
        $p=0;
        $f=0;
        $xml='<customers xlink:href="http://mystore.com/api/products" get="true" put="true"
post="true" delete="true" head="true">
';
        $customers = "http://www.timodna.com.ua/api/products/?display=full";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $customers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERPWD, $this->prestaWebServicesKey . ":");
        curl_setopt($ch, CURLOPT_POST, true);
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        $output = curl_exec($ch);
        $response_array = (array) json_decode($output,true);
        curl_close($ch);
        var_dump($response_array);

    }
    /*

    <prestashop xmlns:xlink="http://www.w3.org/1999/xlink">
        <product>
        <id/>
        <id_manufacturer/>
        <id_supplier/>
        <id_category_default/>
        <new/>
        <cache_default_attribute/>
        <id_default_image/>
        <id_default_combination/>
        <id_tax_rules_group/>
        <position_in_category/>
        <type/>
        <id_shop_default/>
        <reference/>
        <supplier_reference/>
        <location/>
        <width/>
        <height/>
        <depth/>
        <weight/>
        <quantity_discount/>
        <ean13/>
        <isbn/>
        <upc/>
        <cache_is_pack/>
        <cache_has_attachments/>
        <is_virtual/>
        <state/>
        <additional_delivery_times/>
        <delivery_in_stock>
        <language id="1"/>
        <language id="2"/>
        </delivery_in_stock>
        <delivery_out_stock>
        <language id="1"/>
        <language id="2"/>
        </delivery_out_stock>
        <on_sale/>
        <online_only/>
        <ecotax/>
        <minimal_quantity/>
        <low_stock_threshold/>
        <low_stock_alert/>
        <price/>
        <wholesale_price/>
        <unity/>
        <unit_price_ratio/>
        <additional_shipping_cost/>
        <customizable/>
        <text_fields/>
        <uploadable_files/>
        <active/>
        <redirect_type/>
        <id_type_redirected/>
        <available_for_order/>
        <available_date/>
        <show_condition/>
        <condition/>
        <show_price/>
        <indexed/>
        <visibility/>
        <advanced_stock_management/>
        <date_add/>
        <date_upd/>
        <pack_stock_type/>
        <meta_description>
        <language id="1"/>
        <language id="2"/>
        </meta_description>
        <meta_keywords>
        <language id="1"/>
        <language id="2"/>
        </meta_keywords>
        <meta_title>
        <language id="1"/>
        <language id="2"/>
        </meta_title>
        <link_rewrite>
        <language id="1"/>
        <language id="2"/>
        </link_rewrite>
        <name>
        <language id="1"/>
        <language id="2"/>
        </name>
        <description>
        <language id="1"/>
        <language id="2"/>
        </description>
        <description_short>
        <language id="1"/>
        <language id="2"/>
        </description_short>
        <available_now>
        <language id="1"/>
        <language id="2"/>
        </available_now>
        <available_later>
        <language id="1"/>
        <language id="2"/>
        </available_later>
        <associations>
        <categories>
        <category>
        <id/>
        </category>
        </categories>
        <images>
        <image>
        <id/>
        </image>
        </images>
        <combinations>
        <combination>
        <id/>
        </combination>
        </combinations>
        <product_option_values>
        <product_option_value>
        <id/>
        </product_option_value>
        </product_option_values>
        <product_features>
        <product_feature>
        <id/>
        <id_feature_value/>
        </product_feature>
        </product_features>
        <tags>
        <tag>
        <id/>
        </tag>
        </tags>
        <stock_availables>
        <stock_available>
        <id/>
        <id_product_attribute/>
        </stock_available>
        </stock_availables>
        <accessories>
        <product>
        <id/>
        </product>
        </accessories>
        <product_bundle>
        <product>
        <id/>
        <quantity/>
        </product>
        </product_bundle>
        </associations>
        </product>
</prestashop>

      */

}
<?php

namespace App\Controller;

use App\Entity\CategoriesToParse;
use App\Entity\Forms;
//use http\Env\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

use App\Entity\Programs;
use App\Entity\Categories;
use App\Entity\Product;
use Symfony\Component\Serializer\Encoder\JsonDecode;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Require ROLE_USER for *every* controller method in this class.
 *
 * @IsGranted("ROLE_USER")
 */
class CategoriesToParseController extends AbstractController
{
    private $cats_to_parse;

    /**
     * @Route("/catparse", name="catparse")
     */
    public function index(Request $request)
    {

        if($request->get("delprod")){//delete links and products from category

            $this->getDoctrine()->getRepository(CategoriesToParse::class)->deleteProductsAndLinks($request->get("delprod"));
        }

        $this->cats_to_parse = $this->getDoctrine()->getRepository(CategoriesToParse::class)
        ->findAll();

        //*************create new category to parse

        //programs
        $responsePrograms = array();
        $programs = $this->getDoctrine()->getRepository(Programs::class)
            ->findAll();
        foreach ($programs as $program){

            $responsePrograms[$program->getName()] = $program->getId();
        }

        //categories
        $categories = $this->getDoctrine()->getRepository(Categories::class)
            ->findAll();

        $responseCategory["Main"] = "0";
        foreach ($categories as $category){

            $responseCategory[$category->getName()] = $category->getId();
        }

        $form = $this->createFormBuilder()
            ->add('name',TextType::class,array(
                'label' => 'Category Name',
            ))
            ->add('program', ChoiceType::class, array(

                'choices' => $responsePrograms,
            ))
            ->add('link',TextType::class,array(
                'label' => 'Link To Category',
            ))
            ->add('main_category', ChoiceType::class, array(
                'label' => 'Категория timodna',
                'choices' => $responseCategory,
            ))

            ->add('markup',TextType::class,array(
                'label' => 'Наценка, %',
                'data' => '12',
            ))
            ->add('usexml', CheckboxType::class, array(
                'label'    => 'Use XML file',
                'required' => false,
            ))
            ->add('submit', SubmitType::class, array('label' => 'Add Link to Category'))
            ->getForm();


        $form->handleRequest($request);
        if($form->isSubmitted()){

            $newCategory = new CategoriesToParse();
            $data = $form->getData();
            $newCategory->setCatToParseName($data['name']);
            $newCategory->setCatMainId($data['main_category']);
            $newCategory->setDateParsed("11/01/1985");
            $newCategory->setProductCount(0);
            $newCategory->setProgram($data['program']);
            $newCategory->setCatToParseLink($data['link']);
            $newCategory->setMarkup($data['markup'] ?? 10);
            $newCategory->setUseXml( $data['usexml'] ?? 0 );
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newCategory);
            $entityManager->flush();

        }

        $this->cats_to_parse = $this->getDoctrine()->getRepository(CategoriesToParse::class)
            ->findAll();

        return $this->render('categories_to_parse/index.html.twig', [
            'controller_name' => 'CategoriesToParseController',
            'form'=>$form->createView(),
            'cats_to_parse' => $this->cats_to_parse
        ]);
    }

    /**
     * @Route("/catparse-edit-{id}", name="catparse-edit",  requirements={"id"="\d+"})
     */
    public function edit($id, Request $request)
    {

        //create form: get categories to parse for choice selector
        //categories
        $categories = $this->getDoctrine()->getRepository(Categories::class)
            ->findAll();

        $responseCategory["Main"] = "0";
        foreach ($categories as $category){

            $responseCategory[$category->getName()] = $category->getId();
        }

        //get forms for categories
        $forms = $this->getDoctrine()->getRepository(Forms::class)
            ->findAll();
        $responseForms = array();
        foreach ($forms as $category_form){
            $responseForms[$category_form->getName()." | id = ".$category_form->getId().' cat = '.$category_form->getCatToParseId()] = $category_form->getId();
        }
        //programs
        $responsePrograms = array();
        $programs = $this->getDoctrine()->getRepository(Programs::class)
            ->findAll();
        foreach ($programs as $program){

            $responsePrograms[$program->getName()] = $program->getId();
        }
        //get parser form by id
        $category_to_parse = $this->getDoctrine()->getRepository(CategoriesToParse::class)
            ->find($id);

        //our category parameters
        $category_parameters=$this->getDoctrine()->getRepository(Categories::class)->find($category_to_parse->getCatMainId());


        $defaults = array(
            'name'=>$category_to_parse->getCatToParseName(),
            'program'=>$category_to_parse->getProgram(),
            'link'=>$category_to_parse->getCatToParseLink(),
            'main_category'=>$category_to_parse->getCatMainId(),
            'form_id'=>$category_to_parse->getFormId(),
            'markup'=>$category_to_parse->getMarkup(),
            'params_parsed'=>$category_to_parse->getParamsParsed(),
            'params_mapping'=> $category_to_parse->getParamsMapping() ?? $category_to_parse->getParamsParsedFormatted(),
            'our_category_parameters'=> $category_parameters->getParams(),
            'catid'=>$category_to_parse->getId()
        );

        $form = $this->createFormBuilder($defaults)
            ->add('name',TextType::class,array(
                'label' => 'Category Name',
            ))
            ->add('program', ChoiceType::class, array(

                'choices' => $responsePrograms,
            ))
            ->add('link',TextType::class,array(
                'label' => 'Link To Category',
            ))
            ->add('main_category', ChoiceType::class, array(
                'label' => 'Категория timodna',
                'choices' => $responseCategory,
            ))
            ->add('form_id', ChoiceType::class, array(
                'label' => 'Форма категории',
                'choices' => $responseForms,
            ))
            ->add('markup',TextType::class,array(
                'label' => 'Наценка, %'

            ))
            ->add('params_parsed', TextareaType::class, array(
                'label' => 'Parsed Parameters',
                'required' => false,
                'attr' => array('columns' => '20','rows'=>'10','class'=>'form-control-sm')
            ))
            ->add('params_mapping', TextareaType::class, array(
                'label' => 'Parameters Mapping',
                'required' => false,
                'attr' => array('columns' => '20','rows'=>'10','class'=>'form-control-sm')
            ))
            ->add('our_category_parameters', TextareaType::class, array(
                'label' => 'Our Parameters',
                'required' => false,
                'attr' => array('columns' => '20','rows'=>'10','class'=>'form-control-sm')
            ))
            ->add('submit', SubmitType::class, array('label' => 'Update Category'))
            ->add('collectparameters', ButtonType::class, array('label' => 'Collect Parameters'))
            ->add('catid', HiddenType::class, array())
            ->getForm();

        //process request
        $form->handleRequest($request);
        if($form->isSubmitted()){

            $data = $form->getData();
            $category_to_parse->setCatToParseName($data['name']);
            $category_to_parse->setCatMainId($data['main_category']);
            $category_to_parse->setFormId($data['form_id']);
            $category_to_parse->setProgram($data['program']);
            $category_to_parse->setCatToParseLink($data['link']);
            $category_to_parse->setMarkup($data['markup']);
            $category_to_parse->setParamsParsed($data['params_parsed']);
            $category_to_parse->setParamsMapping($data['params_mapping']);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($category_to_parse);
            $entityManager->flush();


        }


        return $this->render('categories_to_parse/edit.html.twig', [
            'controller_name' => 'CategoriesToParseController',
            'form'=>$form->createView()
        ]);

    }
    /**
     * @Route("/collect-params", name="collect-parameters")
     *
     * */
    public function collectParameters(Request $request){


        if($request->get('id')){
            //$category_to_parse = $this->getDoctrine()->getRepository(CategoriesToParse::class)
            //    ->find($request->get('id'));

            $params = array();
            $products = $this->getDoctrine()->getRepository(Product::class)
                ->findBy(['cat_parsed'=>$request->get('id')]);

            foreach ($products as $product)
            {
                $params_product = $product->getDecodedParams();

                foreach ($params_product as $item) {
                    if (!in_array($item->param, $params))
                        array_push($params, $item->param);

                }
            }
            $params = implode("\n",$params);
            return new JsonResponse(['params'=>$params]);
        }

        return new JsonResponse(['params'=>'000']);



    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

use App\Entity\Forms;
use App\Entity\CategoriesToParse;

/**
 * Require ROLE_USER for *every* controller method in this class.
 *
 * @IsGranted("ROLE_USER")
 */
class FormsController extends AbstractController
{

    private $forms;
    /**
     * @Route("/forms", name="forms")
     */
    public function index(Request $request)
    {
        //************add new form
        //create form: get categories to parse for choice selector
        $categories_to_parse = $this->getDoctrine()->getRepository(CategoriesToParse::class)
            ->findAll();
        $responseCategory = array();
        foreach ($categories_to_parse as $category){
            $responseCategory[$category->getCatToParseName()." | id = ".$category->getId()] = $category->getId();
        }

        //form
        $form = $this->createFormBuilder()
            ->add('name',TextType::class,array(
                'label' => 'Form Name',
            ))
            ->add('category', ChoiceType::class, array(

                'choices' => $responseCategory,
            ))
            ->add('submit', SubmitType::class, array('label' => 'Add Form'))
            ->getForm();


        $form->handleRequest($request);
        //process request
        if($form->isSubmitted()){

            $newForm = new Forms();
            $data = $form->getData();
            $newForm->setName($data['name']);
            $newForm->setCatToParseId($data['category']);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($newForm);
            $entityManager->flush();

        }


        //get forms list
        $this->forms = $this->getDoctrine()->getRepository(Forms::class)
            ->findAll();

        return $this->render('forms/index.html.twig', [
            'controller_name' => 'FormsController',
            'form' => $form->createView(),//html form
            'forms' => $this->forms,//form to parse web page
        ]);
    }

    /**
     * @Route("/form-{id}", name="form", requirements={"id"="\d+"})
     */
    public function edit($id, Request $request)
    {

        //create form: get categories to parse for choice selector
        $categories_to_parse = $this->getDoctrine()->getRepository(CategoriesToParse::class)
            ->findAll();
        $responseCategory = array();
        foreach ($categories_to_parse as $category){
            $responseCategory[$category->getCatToParseName()." | id = ".$category->getId()] = $category->getId();
        }
        //get parser form by id
        $parser_form = $this->getDoctrine()->getRepository(Forms::class)
            ->find($id);

        $defaults = array(
            'name'=>$parser_form->getName(),
            'cat_to_parse_id'=>$parser_form->getCatToParseId(),
            'cat_template'=>$parser_form->getCatTemplate(),
            'product_template'=>$parser_form->getProductTemplate(),
            'product_list_template'=>$parser_form->getProductListTemplate(),
        );

        $form = $this->createFormBuilder($defaults)
            ->add('name',TextType::class, array('label'=>'Form Name'))
            ->add('cat_to_parse_id',
                ChoiceType::class, array(
                    'label' => 'Category to parse',
                    'choices' => $responseCategory,
                    'preferred_choices'=>array($parser_form->getCatToParseId())))
            ->add('cat_template',TextareaType::class)
            ->add('product_template', TextareaType::class)
            ->add('product_list_template', TextareaType::class)
            ->add('submit', SubmitType::class, array('label' => 'Update Form'))
            ->getForm();

        //process request
        $form->handleRequest($request);
        if($form->isSubmitted()){
            $data = $form->getData();
            $parser_form->setName($data['name']);
            $parser_form->setCatToParseId($data['cat_to_parse_id']);
            $parser_form->setCatTemplate($data['cat_template']);
            $parser_form->setProductTemplate($data['product_template']);
            $parser_form->setProductListTemplate($data['product_list_template']);

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($parser_form);
            $entityManager->flush();
        }

        return $this->render('forms/edit.html.twig', [
            'controller_name' => 'FormsController',
            'parser_form' => $parser_form,
            'form'=>$form->createView()
        ]);

    }
}

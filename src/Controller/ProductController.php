<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Categories;
use App\Entity\CategoriesToParse;
use http\Env\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;



/**
 * Require ROLE_USER for *every* controller method in this class.
 *
 * @IsGranted("ROLE_USER")
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/product", name="product")
     */
    public function index()
    {
        //get last 50 products

        $products = $this->getDoctrine()->getRepository(Product::class)
           ->findBy(array(),array('id'=>'DESC'),100);
            //->findAll();


        return $this->render('product/index.html.twig', [
            'controller_name' => 'ProductController',
            'products'=>$products
        ]);
    }

    /**
     * @Route("/product-articul", name="product-articul")
     */
    public function setArticul() {

        //set product articuls. Articul = R{id}
        /*
         if model !=null set same articul for same model. model # is imported ( can be model or articul from external website)
         */
        $products = $this->getDoctrine()->getRepository(Product::class)
            ->findBy(['articul'=>NULL]);
        $r=0;//counter for # of created articuls
        foreach ($products as $product){

            if($product->getModel()!=NULL)//check if articul for this model exists, if not - create new Articul
            {
                $program=$this->getDoctrine()->getRepository(Product::class)
                    ->getProgramId($product->getId());
                if($program==2)//Stimma
                {
                    $product2 = $this->getDoctrine()->getRepository(Product::class)
                        ->findOneByName($product->getName());    //var_dump($product2);

                }else//find product by model and articul != NULL
                $product2 = $this->getDoctrine()->getRepository(Product::class)
            ->findOneByModel($product->getModel());    //var_dump($product2);

                //if category STIMMA.com.ua use product name to define articul
                //if($product->getCatParsed()==3)

                if($product2){//if articul exists, use it.
                    $product->setArticul($product2->getArticul());
                }else{//create new articul
                    $product->setArticul("R".$product->getId());
                    $r++;
                }
                $product2=NULL;


            }else{//create new articul
                $product->setArticul("R".$product->getId());
                $r++;
            }

            //update product
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($product);
            $entityManager->flush();

        }


        return new JsonResponse(['numproducts' => count($products),'articuls'=>$r]);

    }

    /**
     * @Route("/product-export-prom", name="product-export-prom")
     */
    public function exportXMLProm()
    {
        $products = $this->getDoctrine()->getRepository(Product::class)
            ->findForProm();
        $prom_products_number = count($products);//number of articuls products for prom
        //get parsed categories for parameters mapping
        $parameters_mapping = $this->getDoctrine()->getRepository(CategoriesToParse::class)->getParametersMapping();
        //get markups
        $markups = $this->getDoctrine()->getRepository(CategoriesToParse::class)->getMarkups();

        //create XML file
        // set up the document
        $xml = new \XMLWriter();
        $xml->openMemory();
        $xml->startDocument('1.0', 'UTF-8');
        $xml->startElement('yml_catalog');
        $xml->writeAttribute('date', date("Y-m-d H:i"));
        $xml->startElement('shop');

        $xml->writeElement('name',"TiModna");//$xml->text("TiModna");$xml->endElement();
        $xml->writeElement('company',"TiModna");//$xml->text("TiModna");$xml->endElement();
        $xml->writeElement('url',"http://www.TiModna.com.ua");//$xml->text("http://TiModna.com.ua");$xml->endElement();
        $xml->writeElement('phone',"0689432488");//$xml->text("0689432488");$xml->endElement();
        $xml->writeElement('platform',"Yandex.YML for OpenCart (ocStore)");//$xml->text("Yandex.YML for OpenCart (ocStore)");$xml->endElement();
        $xml->writeElement('version',"1.7");//$xml->text("1.7");$xml->endElement();
        $xml->startElement('currencies');
        $xml->startElement('currency');
        $xml->writeAttribute('id', "UAH");
        $xml->writeAttribute('rate', "1.00000000");
        $xml->endElement();
        $xml->endElement();

        //categories element

        $xml->startElement('catalog');//categories start

        //get categories
        $categories = $this->getDoctrine()->getRepository(Categories::class)
            ->findAll();
        $categories_array=[];
        $categories_keywords=[];
        foreach($categories as $category) {

                $xml->startElement('category');

                //foreach($ids as $key=>$value)
                $xml->writeAttribute('portal_id', $category->getPromua());
                $xml->writeAttribute('id', $category->getId());
                $xml->writeAttribute('parentId', $category->getParent());
                $xml->text($category->getName());
                $xml->endElement();

                $categories_array[$category->getId()]=$category->getPromua();
                $categories_keywords[$category->getId()]=[$category->getKeywords(),$category->getMetaDescription()];

        }
        $xml->endElement();//categories end

        //export products
        $xml->startElement('items');//offers start
        $prom_products_counter=0;
        foreach ($products as $product) {

            //apply our names for parameters
            $parameters_map = $parameters_mapping[$product["cat_parsed"]];
            $params = json_decode($product["params"]);
            $params_updated = [];
            if ($params) {
                foreach ($params as $param1) {
                    if (isset($parameters_map[trim($param1->param)]))
                        $tmp_param_name = strlen(trim($parameters_map[trim($param1->param)])) > 1 ? trim($parameters_map[trim($param1->param)]) : $param1->param;
                    else
                        $tmp_param_name = $param1->param;
                    $tmp_param_value = $param1->value;
                    $params_updated[]=array('param'=>$tmp_param_name,'value'=>$param1->value);
                    if($tmp_param_name=='Цвет')
                        $product['color'] = $param1->value;
                }

            }


            //build few products for every size $product['sizes'].
            $product['sizes'] = explode(',',$product['sizes']);
            $product["pricenew"] = round($product["price"] * (1+$markups[$product["cat_parsed"]]/100), -1);
            $product["priceopt"] = round($product["price"] * (1+$markups[$product["cat_parsed"]]/100-0.1), -1);

            foreach ($product['sizes'] as $size) {//for Prom, we create different product items for different sizes
                $xml->startElement('item');//offer start

                $xml->writeAttribute('id', $this->getProductPromId($product["id"],$size));// product id - unique for prom, used to update product

                $xml->writeAttribute('available', "true");
                $xml->writeAttribute('group_id', str_ireplace("R", "", $product["articul"]));
                $xml->writeAttribute('selling_type', "u");
                //$xml->writeElement('url',$product['url']);
                $xml->writeElement('price', $product["pricenew"]);
                //$xml->writeElement('oldprice',$product['oldprice']);
                $xml->startElement('prices');
                $xml->startElement('price');
                $xml->writeElement('value', $product['priceopt']);
                $xml->writeElement('quantity', 5);
                $xml->endElement();
                $xml->endElement();
                $xml->writeElement('currencyId', "UAH");
                $xml->writeElement('categoryId', $product["cat_main"]);
                $xml->writeElement('portal_category_id', $categories_array[$product["cat_main"]]);//
                $xml->writeElement('pickup', "false");
                $xml->writeElement('delivery', "true");
                $xml->writeElement('name', $product["name"]);
                $xml->writeElement('keywords', $this->makeMetaKeywords($product,$categories_keywords[$product['cat_main']],$size));
                $xml->writeElement('metaDescription', $this->makeMetaDescription($product,$categories_keywords[$product['cat_main']],$size));
                $xml->writeElement('vendorCode', $product["articul"]);

                $xml->startElement('param');
                $xml->writeAttribute('name', "Размер");
                //$xml->text($product['sizes']);
                $xml->text($size);
                $xml->endElement();

                $xml->startElement('description');
                $xml->writeCData($product["description"]);
                $xml->endElement();

                //images
                $images = json_decode($product["images"]);
                if ($images) {//update product
					//max 10 pictures 
					$images10 = array_slice($images, 0, 10);
                    foreach ($images10 as $image) {
                        $xml->writeElement('image', "http://www.timodna.com.ua/img/pp/" . $image);
                    }
                }

                //parameters
                //images
                /*$params = json_decode($product["params"]);
                if ($params) {//update product
                    foreach ($params as $param1) {                    //var_dump($parameters_map);
                        $xml->startElement('param');
                        if (isset($parameters_map[$param1->param]))
                            $xml->writeAttribute('name', (strlen(trim($parameters_map[trim($param1->param)])) > 1 ? trim($parameters_map[$param1->param]) : $param1->param));
                        else
                            $xml->writeAttribute('name', $param1->param);
                        $xml->text($param1->value);
                        $xml->endElement();
                    }

                }*/
                if ($params_updated) {//update product
                    foreach ($params_updated as $param1) {                    //var_dump($parameters_map);
                        $xml->startElement('param');
                        $xml->writeAttribute('name', $param1['param']);
                        $xml->text($param1['value']);
                        $xml->endElement();
                    }

                }

                $xml->endElement();//offer end
                $prom_products_counter++;
            }//end foreach $product['sizes']
        }

        $xml->endElement();//offers end
        $xml->endElement();
        $xml->endElement();
        $xml->endElement();
		$contents=$xml->outputMemory(true);
        file_put_contents("promua.xml",$contents);
		file_put_contents("/var/www/uamedia/data/www/timodna.com.ua/www/download/promua.xml",$contents);
		

        return new JsonResponse(['link' => "promua.xml", 'products_counter'=>$prom_products_counter,'products_articuls'=>$prom_products_number]);

}
	
	/**
     * @Route("/product-export-tmcomua", name="product-export-tmcomua")
     */
    public function exportWebServicesTimodnaComUa()
    {
		$key = 'JIGGNDFWTEKN7DKGDK95A2DY9QUKB9MS'; //timodna.com.ua webservices key
		$url = 'http://www.timodna.com.ua/api/products?output_format=JSON'; 

		$products = $this->getDoctrine()->getRepository(Product::class)
            ->findForProm();
        //get parsed categories for parameters mapping
        $parameters_mapping = $this->getDoctrine()->getRepository(CategoriesToParse::class)->getParametersMapping();

		$xml='<?xml version="1.0" encoding="UTF-8"?>
<prestashop xmlns:xlink="http://www.w3.org/1999/xlink">
';


		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_USERPWD, $key . ":");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
		$output = curl_exec($ch);
		curl_close($ch);
	}
	/**
     * @Route("/product-csv-tmcomua", name="product-csv-tmcomua")
     */
    public function exportCSVTimodnaComUa()
    {
        $status = 1;//Get only active products
        //For Prestashop: unique key is needed. Select Articuls. After that go through articuls and build products for prestashop
		$articuls = $this->getDoctrine()->getRepository(Product::class)->findArticuls(1);

		$csv = "Product ID;Active (0/1);Name *;Categories (x,y,z...);Price tax included;Tax rules ID;Wholesale price;Reference #;Visibility;Additional shipping cost;Short description;Description;Tags (x,y,z...);Meta title;Meta keywords;Meta description;URL rewritten;Available for order (0 = No, 1 = Yes);Product creation date;Show price (0 = No, 1 = Yes);Image URLs (x,y,z...);Image alt texts (x,y,z...);Delete existing images (0 = No, 1 = Yes);Feature(Name:Value:Position);Available online only (0 = No, 1 = Yes)";
        $combinations = "";
        
		foreach($articuls as $articul){
            $products = $this->getDoctrine()->getRepository(Product::class)->findProductsByArticul($articul['articul']);

            $csv .= "\n".$products[0]['articul'].';1;'.$products[0]['name'];
            foreach ($products as $product) {
                $csv .= "\n".$product;
            }
            //var_dump($products);

        }
		//get parsed categories for parameters mapping
        //$parameters_mapping = $this->getDoctrine()->getRepository(CategoriesToParse::class)->getParametersMapping();
	}
	
/**converts size into integer
 * @return int
 */
public function getProductPromId($productId,$size){
    $array_sizes = str_split($size);
    $chars = "";
	//$i=0;
    foreach($array_sizes as $char){
		$chars.=ord($char);
		//if($i>1) break;
		//$i++;
	}
    return (int)$productId.$chars;
}
/** returns keywords for product. create keywords when exporting, to make for separate sizes,colors.
 *
 * @return string
 */
    public function makeMetaKeywords($product, $categoryKeywords=NULL,$size="") : string
    {
        $product['color'] = isset($product['color']) ? $product['color'] : "";

        if(isset($categoryKeywords)) {
            $keywords = str_ireplace("{name}", $product['name'], $categoryKeywords[0]);
            $keywords = str_ireplace("{size}", $size, $keywords);
            $keywords = str_ireplace("{color}", $product['color'], $keywords);
            $keywords = str_ireplace("{price}", $product['pricenew'], $keywords);


        }else{
            try {
                $keywords = $product['name'] . ' ' . $size . ' ' . $product['color'] . ' ' . $product['pricenew'];
            }catch (\Exception $e)
            {
                $keywords = $product['name'].' '.$product['pricenew'];
            }
        }
        return $keywords;

    }
/** returns meta desc for product. create desc when exporting, to make for separate sizes,colors.
 *
 * @return string
 */
    public function makeMetaDescription($product, $categoryKeywords=NULL,$size="") : string
    {
        $product['color'] = isset($product['color']) ? $product['color'] : "";
        $description = str_ireplace("{name}",$product['name'],$categoryKeywords[1]);
        $description = str_ireplace("{size}",$size,$description);
        $description = str_ireplace("{color}",$product['color'],$description);
        $description = str_ireplace("{price}",$product['pricenew'],$description);

        return html_entity_decode($description);

    }

/** process products, delete older then 1 week non-updated and images
 *  @Route("/product-delete-images", name="product-delete-images")
 * @return mixed
 */
    public function productDeleteImages(){

        $local = "/var/www/uamedia/data/www/timodna.com.ua/www/img/pp/";

        //empty images_timodna not active products ( status!=1)
        $this->getDoctrine()->getRepository(Product::class)->setAllImagesTiModnaNullByStatus();
        //get all active products images
        $active_images = $this->getDoctrine()->getRepository(Product::class)->getUsedImages();

        $ai_links = [];
        $tmp = NULL;
        foreach ($active_images as $image){
            //$ai_ids[]=$image["id"];
            if($image["images_timodna"]==NULL || $image["images_timodna"]=="") continue;

            $tmp=json_decode($image["images_timodna"]);
            foreach ($tmp as $item) {//put all images in 1 level array
                if($item!="")
                $ai_links[]=$item;
            }
            $tmp=NULL;
            //$ai_links[]=json_decode($image["images_timodna"]);
        }
        //get list of images in folder
        $files = scandir($local);
        $files_count = count($files);
        $counter = 0;
        $counter_failed = 0;
        //go through images, if not in active images - delete
        foreach ($files as $file) {
            if($file!="."&&$file!="..")
                if(!in_array($file,$ai_links))
                {
                    try {
                        if(unlink($local . $file))
                            $counter++;
                    }catch (\Exception $exception)
                    {
                        $counter_failed++;

                    }

                }
        }



        return new \Symfony\Component\HttpFoundation\Response(
            '<html><body>Total Files: '.$files_count.'<br>
Files deleted: '.$counter.'<br>
Files failed deletion: '.$counter_failed.'<br>

</body></html>'
        );
    }

}
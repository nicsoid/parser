<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LinksToParseRepository")
 */
class LinksToParse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\Column(type="integer")
     */
    private $cat_main_id;

    /**
     * @ORM\Column(type="integer")
     */
    private $form_id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $status;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_parsed;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $info;

    /**
     * @ORM\Column(type="integer")
     */
    private $category_to_parse;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getCatMainId(): ?int
    {
        return $this->cat_main_id;
    }

    public function setCatMainId(int $cat_main_id): self
    {
        $this->cat_main_id = $cat_main_id;

        return $this;
    }

    public function getFormId(): ?int
    {
        return $this->form_id;
    }

    public function setFormId(int $form_id): self
    {
        $this->form_id = $form_id;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getDateParsed(): ?\DateTimeInterface
    {
        return $this->date_parsed;
    }

    public function setDateParsed(\DateTimeInterface $date_parsed): self
    {
        $this->date_parsed = $date_parsed;

        return $this;
    }

    public function getInfo(): ?string
    {
        return $this->info;
    }

    public function setInfo(?string $info): self
    {
        $this->info = $info;

        return $this;
    }

    public function getCategoryToParse(): ?int
    {
        return $this->category_to_parse;
    }

    public function setCategoryToParse(int $category_to_parse): self
    {
        $this->category_to_parse = $category_to_parse;

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoriesRepository")
 */
class Categories
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $parent;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $promua;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $params;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $Keywords;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $meta_description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $ParamsValues;

    public function __construct()
    {
        $this->Keywords = "Купить {name}, Цена {price}, Размер {size}, Цвет {color}";
        $this->meta_description = "Купить {name} за {price} грн, размер {size}, Цвет {color}";
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getParent(): ?int
    {
        return $this->parent;
    }

    public function setParent(int $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getPromua(): ?int
    {
        return $this->promua;
    }

    public function setPromua(?int $promua): self
    {
        $this->promua = $promua;

        return $this;
    }

    public function getParams(): ?string
    {
        return $this->params;
    }

    public function setParams(?string $params): self
    {
        $this->params = $params;

        return $this;
    }

    public function getKeywords(): ?string
    {
        return $this->Keywords;
    }

    public function setKeywords(?string $Keywords): self
    {
        if($Keywords!="")
            $this->Keywords = $Keywords;
        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->meta_description;
    }

    public function setMetaDescription(?string $meta_description): self
    {
        if($meta_description!="")
            $this->meta_description = $meta_description;

        return $this;
    }

    public function getParamsValues(): ?string
    {
        return $this->ParamsValues;
    }

    public function setParamsValues(?string $ParamsValues): self
    {
        $this->ParamsValues = $ParamsValues;

        return $this;
    }

}

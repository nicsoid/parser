<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $cat_main;

    /**
     * @ORM\Column(type="integer")
     */
    private $cat_parsed;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_parsed;

    /**
     * @ORM\Column(type="integer")
     */
    private $link;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $params;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sizes;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $images;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $model;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $images_timodna;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $status;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $articul;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $keywords;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $meta_description;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $is_new;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $has_discount;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $discount;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getCatMain(): ?int
    {
        return $this->cat_main;
    }

    public function setCatMain(int $cat_main): self
    {
        $this->cat_main = $cat_main;

        return $this;
    }

    public function getCatParsed(): ?int
    {
        return $this->cat_parsed;
    }

    public function setCatParsed(int $cat_parsed): self
    {
        $this->cat_parsed = $cat_parsed;

        return $this;
    }

    public function getDateParsed(): ?\DateTimeInterface
    {
        return $this->date_parsed;
    }

    public function setDateParsed(\DateTimeInterface $date_parsed): self
    {
        $this->date_parsed = $date_parsed;

        return $this;
    }

    public function getLink(): ?int
    {
        return $this->link;
    }

    public function setLink(int $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getParams(): ?string
    {
        return $this->params;
    }

    public function setParams(?string $params): self
    {
        $this->params = $params;

        return $this;
    }

    public function getSizes(): ?string
    {
        return $this->sizes;
    }

    public function setSizes(?string $sizes): self
    {
        $this->sizes = $sizes;

        return $this;
    }

    public function getImages(): ?string
    {
        return $this->images;
    }

    public function setImages(?string $images): self
    {
        $this->images = $images;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDecodedParams()
    {
        return json_decode($this->params);
    }

    public function getModel(): ?string
    {
        return $this->model;
    }

    public function setModel(?string $model): self
    {
        $this->model = $model;

        return $this;
    }

    public function getImagesTimodna(): ?string
    {
        return $this->images_timodna;
    }

    public function setImagesTimodna(?string $images_timodna): self
    {
        $this->images_timodna = $images_timodna;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getArticul(): ?string
    {
        return $this->articul;
    }

    public function setArticul(?string $articul): self
    {
        $this->articul = $articul;

        return $this;
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(?string $keywords): self
    {
        $this->keywords = $keywords;

        return $this;
    }

    public function getMetaDescription(): ?string
    {
        return $this->meta_description;
    }

    public function setMetaDescription(?string $meta_description): self
    {
        $this->meta_description = $meta_description;

        return $this;
    }

    public function getIsNew(): ?bool
    {
        return $this->is_new;
    }

    public function setIsNew(?bool $is_new): self
    {
        $this->is_new = $is_new;

        return $this;
    }

    public function getHasDiscount(): ?bool
    {
        return $this->has_discount;
    }

    public function setHasDiscount(?bool $has_discount): self
    {
        $this->has_discount = $has_discount;

        return $this;
    }

    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    public function setDiscount(?int $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormsRepository")
 */
class Forms
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $cat_to_parse_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $cat_template;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $product_template;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $product_list_template;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCatToParseId(): ?int
    {
        return $this->cat_to_parse_id;
    }

    public function setCatToParseId(int $cat_to_parse_id): self
    {
        $this->cat_to_parse_id = $cat_to_parse_id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCatTemplate(): ?string
    {
        return $this->cat_template;
    }

    public function setCatTemplate(?string $cat_template): self
    {
        $this->cat_template = $cat_template;

        return $this;
    }

    public function getProductTemplate(): ?string
    {
        return $this->product_template;
    }

    public function setProductTemplate(?string $product_template): self
    {
        $this->product_template = $product_template;

        return $this;
    }

    public function getProductListTemplate(): ?string
    {
        return $this->product_list_template;
    }

    public function setProductListTemplate(?string $product_list_template): self
    {
        $this->product_list_template = $product_list_template;

        return $this;
    }
}

<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProgramsRepository")
 */
class Programs
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $website;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_added;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $products_count;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $xml_file;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWebsite(): ?string
    {
        return $this->website;
    }

    public function setWebsite(string $website): self
    {
        $this->website = $website;

        return $this;
    }

    public function getDateAdded(): ?\DateTimeInterface
    {
        return $this->date_added;
    }

    public function setDateAdded(\DateTimeInterface $date_added): self
    {
        $this->date_added = $date_added;

        return $this;
    }

    public function getProductsCount(): ?int
    {
        return $this->products_count;
    }

    public function setProductsCount(?int $products_count): self
    {
        $this->products_count = $products_count;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getXmlFile(): ?string
    {
        return $this->xml_file;
    }

    public function setXmlFile(?string $xml_file): self
    {
        $this->xml_file = $xml_file;

        return $this;
    }
	
	public function getXmlFileContent(): ?string
	{
		//check if file exists and not older then 6h
		$file="/var/www/uamedia/data/www/ourstyle.com.ua/parser/public/".$this->getId().".xml";
		
		if(file_exists($file) && (filectime($file) > ( time()-3600*6 )) )// && (filectime($file) > ( time()-3600*6 )) )
		{
			return file_get_contents($file);
		}else{ //save file from external source
			
			//try few times;
			try{
				$xml = file_get_contents($this->getXmlFile());
				$newfile = fopen($file, "w"); 
                fwrite($newfile, $xml); 
                fclose($newfile);
				return $xml;
			}catch(\Exception $e){
				sleep(10);
				try{
					$xml = file_get_contents($this->getXmlFile());
					$newfile = fopen($file, "w"); 
					fwrite($newfile, $xml); 
					fclose($newfile);
					return $xml;
				}catch(\Exception $e){
					sleep(10);
					try{
						$xml = file_get_contents($this->getXmlFile());
						$newfile = fopen($file, "w"); 
						fwrite($newfile, $xml); 
						fclose($newfile);
						return $xml;
					}catch(\Exception $e){
					return "";
					}
				}	
			}
		}
	}
	
}
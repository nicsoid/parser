<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Entity\Programs;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoriesToParseRepository")
 */
class CategoriesToParse
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cat_to_parse_name;

    /**
     * @ORM\Column(type="integer")
     */
    private $program;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cat_to_parse_link;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $date_parsed;

    /**
     * @ORM\Column(type="integer")
     */
    private $cat_main_id;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $product_count;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $form_id;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $use_xml;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $params_parsed;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $params_mapping;

    /**
     * @ORM\Column(type="float")
     */
    private $markup;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $ParamsValuesParsed;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $params_values_mapping;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCatToParseName(): ?string
    {
        return $this->cat_to_parse_name;
    }

    public function setCatToParseName(string $cat_to_parse_name): self
    {
        $this->cat_to_parse_name = $cat_to_parse_name;

        return $this;
    }

    public function getProgram(): ?int
    {
        return $this->program;
    }

    public function setProgram(int $program): self
    {
        $this->program = $program;

        return $this;
    }

    public function getCatToParseLink(): ?string
    {
        return $this->cat_to_parse_link;
    }

    public function setCatToParseLink(string $cat_to_parse_link): self
    {
        $this->cat_to_parse_link = $cat_to_parse_link;

        return $this;
    }

    public function getDateParsed(): ?string
    {
        return $this->date_parsed;
    }

    public function setDateParsed(string $date_parsed): self
    {
        $this->date_parsed = $date_parsed;

        return $this;
    }

    public function getCatMainId(): ?int
    {
        return $this->cat_main_id;
    }

    public function setCatMainId(int $cat_main_id): self
    {
        $this->cat_main_id = $cat_main_id;

        return $this;
    }

    public function getProductCount(): ?int
    {
        return $this->product_count;
    }

    public function setProductCount(?int $product_count): self
    {
        $this->product_count = $product_count;

        return $this;
    }

    public function getFormId(): ?int
    {
        return $this->form_id;
    }

    public function setFormId(?int $form_id): self
    {
        $this->form_id = $form_id;

        return $this;
    }

    public function getUseXml(): ?bool
    {
        return $this->use_xml;
    }

    public function setUseXml(?bool $use_xml): self
    {
        $this->use_xml = $use_xml;

        return $this;
    }

    public function getParamsParsed(): ?string
    {
        return $this->params_parsed;
    }

    public function setParamsParsed(?string $params_parsed): self
    {
        $this->params_parsed = $params_parsed;

        return $this;
    }

    public function getParamsMapping(): ?string
    {
        return $this->params_mapping;
    }

    public function setParamsMapping(?string $params_mapping): self
    {
        $this->params_mapping = $params_mapping;

        return $this;
    }
    //****

    /**
     *
     */
    public function getParamsParsedFormatted(): string
    {
        $tmp = str_getcsv($this->params_parsed,"\n");
        $tmp2="";

        foreach ($tmp as $value){
            $tmp2.=$value."=>\n";
        }
        $tmp2.="";
        return $tmp2;
    }

    public function getMarkup(): ?float
    {
        return $this->markup;
    }

    public function setMarkup(float $markup): self
    {
        $this->markup = $markup;

        return $this;
    }

    public function getParamsValuesParsed(): ?string
    {
        return $this->ParamsValuesParsed;
    }

    public function setParamsValuesParsed(?string $ParamsValuesParsed): self
    {
        $this->ParamsValuesParsed = $ParamsValuesParsed;

        return $this;
    }

    public function getParamsValuesMapping(): ?string
    {
        return $this->params_values_mapping;
    }

    public function setParamsValuesMapping(?string $params_values_mapping): self
    {
        $this->params_values_mapping = $params_values_mapping;

        return $this;
    }

}
